<?php

add_action( 'after_setup_theme', 'blankslate_setup' );

function blankslate_setup()
{
	load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = 640;
	register_nav_menus(
	array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
	);
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );

function blankslate_load_scripts()
{
	wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );

function blankslate_enqueue_comment_reply_script()
{
	if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );

function blankslate_title( $title ) {
	if ( $title == '' ) {
	return '&rarr;';
	} else {
	return $title;
	}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );

function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );

function blankslate_widgets_init()
{
	register_sidebar( array (
	'name' => __( 'Sidebar Widget Area', 'blankslate' ),
	'id' => 'primary-widget-area',
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => "</li>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
}

function blankslate_custom_pings( $comment )
{
	$GLOBALS['comment'] = $comment;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
	<?php
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );

function blankslate_comments_number( $count )
{
	if ( !is_admin() ) {
	global $id;
	$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
	return count( $comments_by_type['comment'] );
	} else {
	return $count;
	}
}


// //// Register Custom Post Type
// function custom_post_blog() {
// 	  $labels = array(
//         'name'                => _x( 'Blog', 'Post Type Blog', 'camagrifarm' ),
//         'singular_name'       => _x( 'BLog', 'Post Type BLog', 'camagrifarm' ),
//         'menu_name'           => __( 'Blog', 'camagrifarm' ),
//         'parent_item_colon'   => __( 'Blog', 'camagrifarm' ),
//         'all_items'           => __( 'All Blog', 'camagrifarm' ),
//         'view_item'           => __( 'View Blog', 'camagrifarm' ),
//         'add_new_item'        => __( 'Add New Blog', 'camagrifarm' ),
//         'add_new'             => __( 'Add New', 'camagrifarm' ),
//         'edit_item'           => __( 'Edit Blog', 'camagrifarm' ),
//         'update_item'         => __( 'Update Blog', 'camagrifarm' ),
//         'search_items'        => __( 'Search Blog', 'camagrifarm' ),
//         'not_found'           => __( 'Not Found', 'camagrifarm' ),
//         'not_found_in_trash'  => __( 'Not found in Trash', 'camagrifarm' ),
//     );
//
//        $args = array(
//         'label'               => __( 'Blog', 'camagrifarm' ),
//         'description'         => __( 'Blog news and reviews', 'camagrifarm' ),
//         'labels'              => $labels,
//         // Features this CPT supports in Post Editor
//         'supports'            => array( 'title', 'editor', 'Blog', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
//         // You can associate this CPT with a taxonomy or custom taxonomy.
//         'taxonomies'          => array( 'genres' ),
//         /* A hierarchical CPT is like Pages and can have
//         * Parent and child items. A non-hierarchical CPT
//         * is like Posts.
//         */
//         'hierarchical'        => false,
//         'public'              => true,
//         'show_ui'             => true,
//         'show_in_menu'        => true,
//         'show_in_nav_menus'   => true,
//         'show_in_admin_bar'   => true,
//         'menu_position'       => 5,
//         'can_export'          => true,
//         'has_archive'         => true,
//         'exclude_from_search' => false,
//         'publicly_queryable'  => true,
//         'capability_type'     => 'page',
//     );
//
//     // Registering your Custom Post Type
//     register_post_type( 'blog_post', $args );
//
// }
// add_action( 'init', 'custom_post_blog', 0 );





// function add logo header
function themeslug_theme_customizer( $wp_customize ) {
    // Fun code will go here
    $wp_customize->add_section( 'themeslug_logo_section' , array(
    'title'       => __( 'Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
    ) );

    $wp_customize->add_setting( 'themeslug_logo' );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Logo', 'themeslug' ),
    'section'  => 'themeslug_logo_section',
    'settings' => 'themeslug_logo',
) ) );
}
add_action( 'customize_register', 'themeslug_theme_customizer' );


function wpdocs_excerpt_more( $more ) {
    return '<a href="'.get_the_permalink().'" rel="nofollow" class="col-md-12"><button class="btn btn-danger">Read More</button></a>';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/*
* Define a constant path to our single template folder
*/
define(SINGLE_PATH, TEMPLATEPATH . '/single');

/**
* Filter the single_template with our custom function
*/
add_filter('single_template', 'my_single_template');

/**
* Single template function which will choose our template
*/
function my_single_template($single) {
global $wp_query, $post;

/**
* Checks for single template by category
* Check by category slug and ID
*/
foreach((array)get_the_category() as $cat) :

if(file_exists(SINGLE_PATH . '/single-cat-' . $cat->slug . '.php'))
return SINGLE_PATH . '/single-cat-' . $cat->slug . '.php';

elseif(file_exists(SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php'))
return SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php';

endforeach;
}

//register widgets for footer

function tutsplus_widgets_init() {

    // First footer widget area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'First Footer Widget Area', 'tutsplus' ),
        'id' => 'first-footer-widget-area',
        'description' => __( 'The first footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    // Second Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area', 'tutsplus' ),
        'id' => 'second-footer-widget-area',
        'description' => __( 'The second footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    // Third Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Third Footer Widget Area', 'tutsplus' ),
        'id' => 'third-footer-widget-area',
        'description' => __( 'The third footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    // Fourth Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Fourth Footer Widget Area', 'tutsplus' ),
        'id' => 'fourth-footer-widget-area',
        'description' => __( 'The fourth footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

}
add_action('widgets_init','tutsplus_widgets_init');

//limite of search

add_action( 'pre_get_posts', function( $query ) {

    // Check that it is the query we want to change: front-end search query
    if( $query->is_main_query() && ! is_admin() && $query->is_search() ) {

        // Change the query parameters
        $query->set( 'posts_per_page', 3 );

    }

} );


//header option
function header_theme_options($wp_customize) {

 $wp_customize->add_section('Aot_Theme_header_options', array(
        'title' => __('Header Settings', 'camboagrifarm'),
        'priority' => 101,
        'capability' => 'edit_theme_options',
        'description' => __('** You can change Header options Here ** ', 'camboagrifarm'),
            )
    );


		/********************* part of changing text ****************************** */
		////action change

		/*
		 * ******************** part of changing Color title header **************************************
		 */
		////action change
		$wp_customize->add_setting('header_tap_color', array(
				'default' => '#fff',
						)
		);
		//custom text color
    $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize, 'header_tap_color_control', array(
        'label' => __('Header Background Color', 'camboagrifarm'),
        'section' => 'Aot_Theme_header_options',
        'settings' => 'header_tap_color',
        'priority' => 4,
            )
    ));
		////action change
    $wp_customize->add_setting('header_title_color', array(
        'default' => '#ffffff',
            )
    );
    //custom title color
    $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize, 'header_title_color_control', array(
        'label' => __('Header Title Color', 'camboagrifarm'),
        'section' => 'Aot_Theme_header_options',
        'settings' => 'header_title_color',
        'priority' => 3,
            )
    ));

		/*
     * ***************************part of changing Background color **************************************
     */
    ///action change
    $wp_customize->add_setting('header_bg_color', array(
        'default' => '#51a3e9',
            )
    );
    //custom background color
    $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize, 'header_bg_color_control', array(
        'label' => __('Header Menu Background Color', 'camboagrifarm'),
        'section' => 'Aot_Theme_header_options',
        'settings' => 'header_bg_color',
        'priority' => 10,
            )
    ));
		/* *********************** part of border background header ***********************/
		///action change
		$wp_customize->add_setting('header_topbg_color', array(
				'default' => '#F0F2F5',
						)
		);
		//custom border color header
		$wp_customize->add_control(new WP_Customize_Color_Control(
						$wp_customize, 'header_topbg_color_control', array(
				'label' => __('Header Top Background', 'camboagrifarm'),
				'section' => 'Aot_Theme_header_options',
				'settings' => 'header_topbg_color',
						)
		));
		/* *********************** part of border background header ***********************/
		///action change
		$wp_customize->add_setting('header_bor_color', array(
				'default' => '#2a7bd0',
						)
		);
		//custom border color header
		$wp_customize->add_control(new WP_Customize_Color_Control(
						$wp_customize, 'header_bor_color_control', array(
				'label' => __('Header Border Color', 'camboagrifarm'),
				'section' => 'Aot_Theme_header_options',
				'settings' => 'header_bor_color',
						)
		));

		/* ********************************* part of contact argent backgrond and border color *************** */

		///action change
		$wp_customize->add_setting('header_agentbg_color', array(
				'default' => '#9cce3d',
						)
		);
		//custom border and background color contact argent header
		$wp_customize->add_control(new WP_Customize_Color_Control(
						$wp_customize, 'header_agentbg_color_control', array(
				'label' => __('Contact Agent Background Color', 'camboagrifarm'),
				'section' => 'Aot_Theme_header_options',
				'settings' => 'header_agentbg_color',
						)
		));

		/* ======================== section ======================== */
    $wp_customize -> add_section('section_top_bar', array(
        'title'     => 'Top Bar',
        'priority'  => 1
    ));
		$wp_customize -> add_setting('top_bar_phone_no', array(
        'default'   => '',
        'transport' => 'refresh',
    ));
    $wp_customize -> add_control('top_bar_phone_no', array(
        'section'   => 'section_top_bar',
        'label'     => 'Phone Number',
        'type'      => 'option',
    ));
    $wp_customize -> add_setting('top_bar_email', array(
        'default'   => '',
        'transport' => 'refresh',
    ));
    $wp_customize -> add_control('top_bar_email', array(
        'section'   => 'section_top_bar',
        'label'     => 'Email Address',
        'type'      => 'text',
    ));
		/* ******************************** socila part ************************************ */
		for($i=1;$i<=5;$i++) {
        $wp_customize -> add_setting('top_bar_social_'.$i.'_icon', array(
            'default'   => '',
            'transport' => 'refresh',
        ));
        $wp_customize -> add_control('top_bar_social_'.$i.'_icon', array(
            'section'   => 'section_top_bar',
            'label'     => 'Social '.$i.' - Icon Code',
            'type'      => 'text',
        ));
        $wp_customize -> add_setting('top_bar_social_'.$i.'_url', array(
            'default'   => '',
            'transport' => 'refresh',
        ));
        $wp_customize -> add_control('top_bar_social_'.$i.'_url', array(
            'section'   => 'section_top_bar',
            'label'     => 'Social '.$i.' - URL',
            'type'      => 'text',
        ));

    }

		/* ======================== panel three posts ======================== */
		$wp_customize->add_panel('panel_three_posts',
				array(
						'priority' => 6,
						'capability' => 'edit_theme_options',
						'theme_supports' => '',
						'title' => 'Three Posts',
				)
		);
		for($i=1;$i<=3;$i++) {
				$wp_customize -> add_section('post_'.$i, array(
						'title'     => 'Post '.$i,
						'priority'  => 2,
						'panel' => 'panel_three_posts',
				));
				$wp_customize -> add_setting('post_title_'.$i, array(
						'default'   => '',
						'transport' => 'refresh',
				));
				$wp_customize -> add_control('post_title_'.$i, array(
						'section'   => 'post_'.$i,
						'label'     => 'Title',
						'type'      => 'text',
				));
				$wp_customize -> add_setting('post_desc_'.$i, array(
						'default'   => '',
						'transport' => 'refresh',
				));
				$wp_customize -> add_control('post_desc_'.$i, array(
						'section'   => 'post_'.$i,
						'label'     => 'Description',
						'type'      => 'textarea',
				));
				$wp_customize -> add_setting('post_icon_'.$i, array(
						'default'   => '',
						'transport' => 'refresh',
				));
				$wp_customize -> add_control('post_icon_'.$i, array(
						'section'   => 'post_'.$i,
						'label'     => 'Icon Name',
						'type'      => 'text',
				));
		}

}
add_action('customize_register', 'header_theme_options');


function header_dynamic_css() {
    ?>
    <style type='text/css'>
        /* site header style*/
      .sticky-wrapper #nav-section .nav-background {
				background: <?php echo get_theme_mod('header_bg_color'); ?>;

			}
			#top-bar {
				background-color:  <?php echo get_theme_mod('header_topbg_color'); ?> !important;
			}
			.navbar-nav > li > a {
				color: <?php echo get_theme_mod('header_title_color'); ?> !important ;
			}
			.navbar-header .btn {
				color: <?php echo get_theme_mod('header_title_color'); ?>  ;
			}
			#nav-section a {
				color: <?php echo get_theme_mod('header_title_color'); ?>  ;
			}
			.special-actions .btn-primary {
				background-color : <?php echo get_theme_mod('header_bg_color'); ?> !important;
				border-color : <?php echo get_theme_mod('header_bor_color'); ?> !important;
			}
			.header-default .special-actions .fa {
				background-color : <?php echo get_theme_mod('header_bg_color'); ?> !important;
				border :3px solid <?php echo get_theme_mod('header_bor_color'); ?> !important;
			}
			.btn-success {
				background-color: <?php echo get_theme_mod('header_agentbg_color'); ?> !important;
    		border-color: <?php echo get_theme_mod('header_agentbg_color'); ?> !important;
			}
			.btn-primary {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
    		border-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.form-title-display {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			#header {
				background-color: <?php echo get_theme_mod('header_tap_color'); ?> ;
			}
			/* style add three posts */
			.iconBox .iconBox-icon:before {
				border: 2px solid <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.iconBox .iconBox-icon .fa {
				color : <?php echo get_theme_mod('header_bg_color'); ?>  !important;
			}
			.xpro-thumb-selected{
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.iconBox:hover .iconBox-icon {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.iconBox:hover .iconBox-icon .fa{
				color: #fff !important;
			}
			.owl-center-nav .owl-nav .owl-next, .owl-center-nav .owl-nav .owl-prev {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
				border-bottom: 3px solid <?php echo get_theme_mod('header_bor_color'); ?> !important;
			}
			.to-let {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.featured-property .property-bottom-info {
				background-color:<?php echo get_theme_mod('header_bg_color'); ?> !important;
    		border-bottom: 3px solid <?php echo get_theme_mod('header_bor_color'); ?> !important;
			}
			.to-let:before {
				border-color: transparent <?php echo get_theme_mod('header_bg_color'); ?> transparent transparent !important;
			}
			.to-let:after {
				border-color: transparent transparent transparent <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.section-title-border:after {
				    background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.btn-danger {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
				border-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			#copyright .t-color {
				color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			/* land and sale style */
			.grid-style .property-bottom-info {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
				border-color: 3px solid <?php echo get_theme_mod('header_bor_color'); ?> !important;;
			}
			#searchsubmit{
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}
			.view-mode ul li:hover, .sort ul li:hover, .view-mode ul li.active, .sort ul li.active {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
				border-color: 3px solid <?php echo get_theme_mod('header_bg_color'); ?> !important;;
			}
			#submit {
				background-color: <?php echo get_theme_mod('header_bg_color'); ?> !important;
			}

    </style>

    <?php
}

add_action('wp_head', 'header_dynamic_css');
