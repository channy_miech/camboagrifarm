<!DOCTYPE html>
<html lang="en-us">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
<!-- Page Title -->
<title><?php bloginfo(); ?></title>
<meta name="keywords" content="wishhouse, wish house, responsive, modern html5 template, bootstrap, css3, property, estate, real estate agent" />
<meta name="description" content="Wish House- Real Estate HTML5 Responsive Template for Real Estate company" />
<meta name="author" content="Colorful Design - www.bycolorfuldesign.com" />

<!-- Mobile Meta Tag -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<!-- Fav and touch icons -->
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url') ?>/assets/images/fav_touch_icons/favicon.ico" />
<link rel="apple-touch-icon" href="<?php bloginfo('template_url') ?>/assets/images/fav_touch_icons/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url') ?>/assets/images/fav_touch_icons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url') ?>/assets/images/fav_touch_icons/apple-touch-icon-114x114.png" />

<!-- Google Web Font -->
<link href="https://fonts.googleapis.com/css?family=Arimo:400,400i,700,700i&ampCabin:400,400i,500,500i,600,600i,700&ampLato:400,400i,700,700i,900&ampOpen+Sans:400,400i,600,600i,700,700i&ampOswald:300,400,500,600,700&ampPoppins:300,400,500,600,700&ampRaleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&ampVarela+Round&amp;subset=latin-ext" rel="stylesheet" type="text/css">

<!-- Fonf Awesome -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/font-awesome.min.css" />
<!-- Real estate Flaticon Font -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/realestate-flaticon.css" />

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/js/datepiker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/bootstrap-progressbar.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/bootstrap-select.css" />


<!-- ********************** XPRO Sliders ******************-->
<!-- HERO Home Slider -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/js/xpro/slider.css" />

<!-- ********************** Owl Carousel ******************-->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/owl.carousel.css" />

<!-- Template CSS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/style.css" />

</head>
<body class="home-page" >
	<!-- Page loader -->
	<div id="pageloader">
		<div class="sk-cube-grid">
			<div class="sk-cube sk-cube1"></div>
			<div class="sk-cube sk-cube2"></div>
			<div class="sk-cube sk-cube3"></div>
			<div class="sk-cube sk-cube4"></div>
			<div class="sk-cube sk-cube5"></div>
			<div class="sk-cube sk-cube6"></div>
			<div class="sk-cube sk-cube7"></div>
			<div class="sk-cube sk-cube8"></div>
			<div class="sk-cube sk-cube9"></div>
		</div>
	</div>
	<!--  WRAPPER -->
	<div id="wrapper">
	<!-- Mobile menu overlay background -->
	<div class="page-overlay"></div>

	<!-- BEGIN HEADER -->
	  <header id="header" class="header-default">
                    <div id="top-bar">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul id="top-info">
                                        <li><i class="fa  fa-phone fa-2"></i> <?php echo get_theme_mod('top_bar_phone_no'); ?></li>
                                        <li><i class="fa  fa-envelope fa-2"></i> <a href="<?php echo get_theme_mod('top_bar_email'); ?>"><?php echo get_theme_mod('top_bar_email'); ?></a></li>

                                    </ul>

                                    <ul class="socials-box pull-right">
                                        <?php
                                            // $args = array(
                                            //   'theme_location' => 'social-menu',
                                            //   'container'   => 'nav',
                                            //   'container_class' => 'socials',
                                            //   'container_id' => 'socials',
                                            //   'link_before' => '<span class="sr-text"  >',
                                            //   'link_after' => '</span>'
                                            // );
                                            // wp_nav_menu($args);
                                         ?>
																				 <li><a href="<?php echo get_theme_mod('top_bar_social_1_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_1_icon'); ?> fa-2"></i></a></li>
																				 <li><a href="<?php echo get_theme_mod('top_bar_social_2_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_2_icon'); ?> fa-2"></i></a></li>
																				 <li><a href="<?php echo get_theme_mod('top_bar_social_3_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_3_icon'); ?> fa-2"></i></a></li>
																				 <li><a href="<?php echo get_theme_mod('top_bar_social_4_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_4_icon'); ?> fa-2"></i></a></li>
																				 <li><a href="<?php echo get_theme_mod('top_bar_social_5_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_5_icon'); ?> fa-2"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container navbar-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3 header-left">
                                    <?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
                                              <a class="logo" href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>

                                      <?php else : ?>
                                          <hgroup>
                                              <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
                                              <h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
                                          </hgroup>
                                      <?php endif; ?>
                                </div>
                                <div class="col-md-9 header-right">

                                     <a class="btn  btn-success btn-sm" href="<?php echo esc_url( home_url( '/?page_id=206' ) ); ?>">Contact Agents <i class="fa fa-chevron-right"></i></a>
                                    <a class="btn btn-primary btn-sm" href="<?php echo esc_url( home_url( '/?page_id=178' ) ); ?>">Viewing Land And Farm <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button class="nav-mobile-btn is-closed animated fadeInLeft" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>

                    <button class="search-mobile-btn s-is-closed animated fadeInRight" data-toggle="s-offcanvas">
                        <i class="fa fa-search"></i>
                    </button>

                    <div id="nav-section">
                        <div class="nav-background">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN MAIN MENU -->
                                            <div class="special-actions">
                                                <a class="btn btn-primary" href="#"><i class="fa fa-plus"></i>Land And Farm</a>
                                            </div>

                                              <?php
                                                      $args = array(
                                                          'theme_location' => 'header-menu',
                                                          'menu_class'     => 'nav navbar-nav',
                                                          'container'   => 'nav',
                                                          'container_class' => 'navbar'
                                                      );

                                                      wp_nav_menu($args);
                                                     ?>
                                        <!-- END MAIN MENU -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
		<!-- END HEADER -->
