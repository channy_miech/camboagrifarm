<?php get_header(); ?>
<!-- BEGIN HOME SLIDER -->
<div class="xpro-slider-container">
    <div id="scroller" class="xpro-slider" data-minHeight="451px">
        <div class="xpro-slider-content">
          <?php
               $args = array('post_type' => 'slide_show', 'posts_per_page' => 10);
               $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();
          ?>
            <!--ON Slider Item -->
            <?php $image  = get_post_meta($post->ID, 'wpcf-slide-show-image', true);  ?>
            <div class="xpro-slider-item" style="background-image:url('<?php  echo $image ;  ?>');background-position:center center">

                <div class="xpro-item-layer p-slide-overlay" data-effect="animate({'delay':'0.5s', 'duration':'1s'})"></div>
                <div class="xpro-item-layer xp-topText-box" data-effect="animate({'Y':'30px', 'delay':'1.5s', 'duration':'600ms'})">
                    <h2 class="xpro-item-layer-responsive xp-headtext"><?php the_title() ; ?></h2>
                    <div class="xpro-item-layer-responsive xp-headsubtext">
                        <!-- Best Deals in One Place -->
                    </div>
                </div>
                <div class="xpro-item-layer xp-bottomText-box" data-effect="animate({'Y':'0px', 'delay':'1.8s', 'duration':'600ms'})">
                    <!-- <h4 class="xpro-item-layer-responsive xp-headtext">Instruct us to sell for <span>$799</span> | Landlords save <span>$1,000’s</span></h4> -->
                    <div class="xpro-item-layer-responsive xp-headsubtext"><?php the_content() ; ?>
                    </div>
                </div>
               <div class="xpro-item-layer xp-buttonBox" data-effect="animate({'Y':'20px', zoom:'in', 'delay':'1.6s', 'duration':'600ms'})">
                    <a href="<?php echo esc_url( home_url( '/?page_id=206' ) ); ?>" class="xpro-item-layer-responsive btn btn-primary btn-lg">Contact Agents <i class="fa fa-chevron-right"></i></a>
                </div>
                <div class="xpro-item-layer xp-ratingBox" data-effect="animate({'Y':'20px', zoom:'in', 'delay':'1.6s', 'duration':'600ms'})">

                </div>
                <div class="xpro-item-layer xp-videoBtn" data-effect="animate({'Y':'20px', zoom:'in', 'delay':'1s', 'duration':'600ms'})">
                    <div class="xpro-video-item xp-video-item-thumb  xpro-item-layer-responsive" data-display="lightbox" data-videourl="<?php  echo get_post_meta($post->ID, 'wpcf-slide-show-url', true); ?>">
                    </div>
                </div>
            </div> <!--OFF Slider Item -->
          <?php endwhile ; ?>
        </div>
    </div>
</div>
<!-- END HOME SLIDER -->
<!-- BEGIN SEARCH FILTER  -->
<section class="search-module">
    <div class="container">
        <div class="form-decor-shadow"></div><!-- decoration -->
        <div class="search-module-inside">
          <form class="form-action form-dark" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
                <div class="form-title-section text-center">
                    <div class="form-title-display">
                        <div class="form-group-btn mobSearch-btn">
                            <button class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                        <div class="form-title text-center">
                            <span class="text-uppercase">Search for land and farm</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row box-action-one">
                        <div class="col-md-3 col-sm-2 cs-padding" style="margin-left:16px;">
                            <label for="s">Location</label>
                            <input type="text" name="s" id="s" class="form-control" value ="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php esc_attr_e( 'location &hellip;', 'shape' ); ?>" required/>
                            <input type="hidden" value="6" name="cat" id="scat" />
                        </div>
                        <div class="col-md-3 col-sm-2 cs-padding">
                            <label for="s2">Title</label>
                            <input type="text" class="field" name="s2" value="<?php echo esc_attr( get_search_query() ); ?>" id="s2" placeholder="<?php esc_attr_e( 'Title &hellip;', 'shape' ); ?>" required />
                        </div>
                        <div class="col-md-3 col-sm-3 cs-padding">
                            <label for="s3">Price</label>
                              <input type="text" class="field" name="s3" value="<?php echo esc_attr( get_search_query() ); ?>" id="s3" placeholder="<?php esc_attr_e( 'Price &hellip;', 'shape' ); ?>" required />
                        </div>
                        <div class="col-md-3 col-sm-2 cs-padding">
                            <!-- Date field ! -->
                            <label for="s5">Status</label>
                              <input type="text" class="field" name="s5" value="<?php echo esc_attr( get_search_query() ); ?>" id="s5" placeholder="<?php esc_attr_e( 'Status &hellip;', 'shape' ); ?>" required />
                        </div>
                        <div class="col-md-2 col-sm-2 cs-padding">
                          <input type="submit"  class="submit btn btn-primary btn-sm cs-search-btn" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'shape' ); ?>" style="background:#53a5ec; color:white; width: 100%;"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- END SEARCH FILTER  -->
<!-- BEGIN ICON BOXES  -->
<section class="main-iconBox edit-paddingBottom10">
    <div class="container">
        <div class="row">
            <div class="col-md-4 edit-marginBottom50">
                <div class="iconBox">
                    <a href="#"><div class="iconBox-icon center-block edit-marginBottom30"><i class="<?php echo get_theme_mod('post_icon_1'); ?>"></i></div></a>
                    <div class="iconBox-description">
                        <a href="#"><span class="iconBox-title edit-marginBottom10"><?php echo get_theme_mod('post_title_1'); ?></span></a>
                        <span class="iconBox-text"><?php echo get_theme_mod('post_desc_1'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 edit-marginBottom50">
                <div class="iconBox">
                    <a href="#"><div class="iconBox-icon center-block edit-marginBottom30"><i class="<?php echo get_theme_mod('post_icon_2'); ?>"></i></div></a>
                    <div class="iconBox-description">
                        <a href="#"><span class="iconBox-title edit-marginBottom10"><?php echo get_theme_mod('post_title_2'); ?></span></a>
                        <span class="iconBox-text"><?php echo get_theme_mod('post_desc_2'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 edit-marginBottom50">
                <div class="iconBox">
                    <a href="#"><div class="iconBox-icon center-block edit-marginBottom30"><i class="<?php echo get_theme_mod('post_icon_3'); ?>"></i></div></a>
                    <div class="iconBox-description">
                        <a href="#"><span class="iconBox-title edit-marginBottom10"><?php echo get_theme_mod('post_title_3'); ?></span></a>
                        <span class="iconBox-text"><?php echo get_theme_mod('post_desc_3'); ?></span>
                    </div>
                </div>
            </div>
        </div><!-- END ROW  -->
    </div>
</section>
<!-- END ICON BOXES  -->
<div class="container">
    <div class="divider col-sm-12"></div>
</div>
<!-- BEGIN FEATURED PROPERTY  -->
<section class="featured-property">
    <div class="container">
        <h3 class="section-title-border">Feature Land And Farm</h3>
        <div class="edit-marginTop40 parallax-box ct-parallax-bg-img" data-image-src="<?php bloginfo('template_url') ?>/assets/images/content/featured-parallax.jpg">
            <div class="mediaSection-box">
                <div class="owl-carousel owl-featured owl-center-nav edit-paddingBoth20">
                  <?php
                  $args = array(
                    'post_type' => 'land-and-farm',
                    'posts_per_page' => 10,
                    'tax_query' => array(
                    array(
                      'taxonomy' => 'category',
                      'field'    => 'slug',
                      'terms'    => array( 'farm-sale', 'land-sale' ),
                    ),
                    ),
                    );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                 ?>
                    <div class="item"><!-- On item  -->
                        <div class="item-box">
                            <label class="decor-label to-let"><?php  echo get_post_meta($post->ID, 'wpcf-status-item', true); ?></label>
                            <a href="<?php the_permalink(); ?>">
                                <div class="main-content">
                                    <div class="image-box">
                                        <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-home-image', true); ?>"  style="width:100%;height:200px;">
                                        <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                    </div>
                                    <div class="main-info">
                                        <div class="property-tilte">
                                          <?php echo mb_strimwidth(get_the_title(), 0, 25, "..."); ?>
                                        </div>
                                        <div class="property-price">
                                          <span> <?php  echo get_post_meta($post->ID, 'wpcf-item-price', true); ?> $</span>
                                        </div>
                                        <hr>
                                        <div class="property-desc">
                                          <p>Address: <?php echo mb_strimwidth(get_post_meta($post->ID, 'wpcf-address', true), 0, 20, "..."); ?></p>
                                          <p><?php echo mb_strimwidth(get_the_content(), 0, 100, "..."); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="property-bottom-info">
                                <div class="text">

                                </div>
                            </div>
                    </div><!-- OFF item  -->

                </div>
                <?php endwhile ; ?>
            </div>
        </div>
    </div>
</section>
<!-- END FEATURED PROPERTY  -->

<!-- BEGIN PROPERTY EXPERT  -->
<section class="edit-padding0 edit-paddingTop20">
  <div class="container">
    <h3 class="section-title-border">Agent Experts</h3>
  </div>
  <div class="property-expert ct-background-color" data-bgcolor="#fbfbfb" data-borderTop="1px solid #e2e2e2" data-borderBottom="1px solid #e2e2e2">
  <div class="container">
    <div class="row">
     <div class="owl-carousel owl-property-expert owl-top-nav"> <!-- On special-menu list  -->
       <?php
            $args = array('post_type' => 'agents_experts', 'posts_per_page' => 10,'category_name' => 'Our Agents');
            $loop = new WP_Query($args);
             while ($loop->have_posts()) : $loop->the_post();
      ?>
        <div class="item"><!-- On item  -->
          <div class="item-box">
            <div class="col-md-4">
              <div class="image">
                 <?php if (has_post_thumbnail()) : ?>
                  <?php the_post_thumbnail(); ?>
               <?php endif; ?>
             </div>
            </div>
            <div class="col-md-4">
                <h4 class="title"><?php  echo get_post_meta($post->ID, 'wpcf-full-name', true); ?></h4>
                <ul class="socials-box pull-left">
                  <li><a href="<?php echo get_theme_mod('top_bar_social_1_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_1_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_2_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_2_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_3_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_3_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_4_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_4_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_5_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_5_icon'); ?> fa-2"></i></a></li>
                </ul>
                <div class="progress-boxes">
                  <h6 class="title">Expensive Property</h6>
                  <div class="progress progress-e">
                    <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="<?php  echo get_post_meta($post->ID, 'wpcf-expensive-property', true); ?>"></div>
                  </div>
                  <h6 class="title">Relatively cheap Properties</h6>
                  <div class="progress progress-e">
                    <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="<?php  echo get_post_meta($post->ID, 'wpcf-relatively-cheap-properties', true); ?>"></div>
                  </div>
                  <h6 class="title">Cheap valuable rental properties</h6>
                  <div class="progress progress-e">
                    <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="<?php  echo get_post_meta($post->ID, 'wpcf-cheap-valuable-rental-properties', true); ?>"></div>
                  </div>
                </div>
            </div>
            <div class="col-md-4">

                <p class="center-text"><?php the_content() ;  ?> </p>
            </div>
          </div>
        </div><!-- OFF item  -->
      <?php endwhile ; ?>
      </div>
    </div>
  </div>
</div>
</section>
<!-- END PROPERTY EXPERT  -->

<!-- BEGIN FROM THE BLOG  -->
<section class="col-md-12" style="padding-bottom: 0px;">
    <div class="container">
        <div class="flex-row row">
            <div class="main col-md-12">
                <h3 class="section-title-border">From the Blog</h3>
                <div class="articleBox edit-marginBottom40 edit-marginTop20">
                     <?php
                   $args = array('post_type' => 'post', 'posts_per_page' => 3, 'category_name'=>'news');
                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) : $loop->the_post();
                ?>
                    <div class="articleBox-body"><!-- ON article -->
                        <a href="#"><div class="articleBox-media" style="height:250px;">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail(); ?>
                            <?php endif; ?>
                                <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                            </div></a>
                        <div class="articleBox-info">
                            <div class="articleBox-titles">
                                <div class="articleTop-info">
                                    <span><i class="fa fa-calendar"></i> <?php echo date(get_option('date_format')); ?></span> <span><i class="fa fa-comments-o"></i> <?php printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( get_comments_number() ) ); ?></span>

                                </div>
                                <a href="#"><?php the_title(); ?></a>
                            </div>
                            <div class="articleBox-description">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END FROM THE BLOG -->

<!-- BEGIN OUR PAERTNERS  -->
<section class="col-md-12" style="padding-top: 0px;">
    <div class="container">
        <h2 class="section-title-border">Our Partners</h2>
        <div class="edit-marginTop40">
            <ul class="owl-carousel owl-ourPartners owl-center-nav">
                 <?php
                       $args = array('post_type' => 'post', 'posts_per_page' => 10, 'category_name'=>'Our Partner');
                       $loop = new WP_Query($args);
                       while ($loop->have_posts()) : $loop->the_post();
                 ?>
                     <li>
                         <a href="#">
                         <?php if (has_post_thumbnail()) : ?>
                             <?php the_post_thumbnail(); ?>
                        <?php endif; ?>
                        </a>
                     </li>

                  <?php endwhile; ?>
            </ul>
        </div>
    </div>
</section>
<!-- END OUR PAERTNERS  -->

<?php get_footer() ; ?>
