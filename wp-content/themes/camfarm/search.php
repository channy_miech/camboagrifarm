
<?php get_header();
global $wp_query;
?>

<!-- BEGIN HEADER BACKGROUND -->
<?php $backgroundImg = get_post_meta($post->ID, 'wpcf-header-image', true); ?>
<div class="header-background-box half">
    <div class="header-background"   style="background: url('<?php echo $backgroundImg;
no - repeat; ?>') ;height: 194px;" >
        <div class="webkit-and-moz-overlay-background">
            <div class="container">
                <div class="center-section">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER BACKGROUND -->
<!-- BEGIN SITE MAP -->
<div class="site-map">
    <div class="container">
        <a href="<?php echo esc_url(home_url('/')); ?>">Home</a>
        <a href="<?php echo esc_url(home_url('/?page_id=178')); ?>"><?php the_title(); ?></a>
    </div>
</div>
<!-- END SITE MAP -->
<!-- BEGIN PROPERTIES  -->
<section class="properties-items">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-8">
        <div class="sorting-bar clearfix">
          <div class="col-md-3 col-sm-3 form-sort-control">
              <select name="sort-by" class="cs-select form-control show-tick selectpicker">
                <option>Most Recent</option>
                <option>Price High to Low</option>
                <option>Price Low to High</option>
              </select>
          </div>
          <div class="view-mode">
            <span>View Mode:</span>
            <ul>
              <li class="active" id="p-showTiles"><i class="fa fa-th"></i></li>
              <li id="p-showList"  ><i class="fa fa-th-list"></i></li>
            </ul>
          </div>
        </div>
        <div id="property-listing" class="flex-row row grid-style">

          <?php if ( have_posts() ) { ?>
              <?php while ( have_posts() ) { the_post(); ?>
          <div class="item col-md-4"><!-- On item  -->
            <div class="item-box">
              <label class="decor-label to-let"><?php  echo get_post_meta($post->ID, 'wpcf-status-item', true); ?></label>
                <a href="<?php the_permalink(); ?>">
                  <div class="main-content">
                      <div class="image-box">
                        <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-home-image', true); ?>"  style="width:100%;height:200px;">

                         <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                      </div>
                      <div class="main-info">
                          <div class="property-tilte">
                            <?php echo mb_strimwidth(get_the_title(), 0, 25, "..."); ?>
                          </div>
                          <div class="property-price">
                              <span> <?php  echo get_post_meta($post->ID, 'wpcf-item-price', true); ?> $</span>
                            <!-- <span class="pull-right"><p><?php // echo get_post_meta($post->ID, 'wpcf-address', true); ?></p></span> -->
                          </div>
                          <hr>
                          <div class="property-desc">
                            <p>Address: <?php echo mb_strimwidth(get_post_meta($post->ID, 'wpcf-address', true), 0, 20, "..."); ?></p>
                            <p><?php echo mb_strimwidth(get_the_content(), 0, 100, "..."); ?></p>
                          </div>
                          <hr>

                      </div>
                  </div>
                </a>
                <div class="property-bottom-info">
                    <div class="text">
                        <span><?php  echo get_post_meta($post->ID, 'wpcf-size', true); ?></span>
                        <span><i class="fa fa-heart-o"></i></span>
                    </div>
                </div>
            </div>
          </div><!-- OFF item  -->
        <?php } ?>
        <div class="clearfix"></div>
        <div class="pagination">
          <?php echo paginate_links(); ?>
        </div>

    <?php }else{ echo "Not Found Data!!!" ;} ?>

        </div>

      </div><!-- OFF col-md-9 -->

      <div class="col-md-3 col-sm-4">
        <aside>
          <!-- BEGIN SEARCH FILTER  -->
            <div class="edit-marginBottom30 edit-marginTop60">
              <!-- include search form -->
              <?php include 'searchform.php'; ?>
          <!-- END SEARCH FILTER  -->

          <div class="sb-latest-posts sb-featured edit-margin0">
            <h4 class="widget-title">Recent Properties</h4>
            <ul>
              <li><a href="#">
                <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                <div class="text-box">
                  <span class="title">Spacious 9 Bedroom villa for sale</span>
                  <div class="property-price">
                      <span class="old-price">$950,000</span>
                      <span>$850,000</span>
                  </div>
                </div>
              </a></li>

              <li><a href="#">
                <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                <div class="text-box">
                  <span class="title">Spacious 8 Bedroom villa for sale</span>
                  <div class="property-price">
                      <span>$800,000</span>
                  </div>
                </div>
              </a></li>
            </ul>
          </div>

          <div class="sb-latest-posts edit-margin0">
            <h4 class="widget-title">Latest News</h4>
            <ul>
              <li><a href="#">
                <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                <div class="text-box">
                  <span class="title">Creamy Checken Pesto Pasta</span>
                  <span class="date">December 6, 2016</span>
                </div>
              </a></li>

              <li><a href="#">
                <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                <div class="text-box">
                  <span class="title">Top 15 Best Restaurants</span>
                  <span class="date">December 6, 2016</span>
                </div>
              </a></li>
            </ul>
          </div>

          <div class="sb-advert edit-marginTop20">
            <div class="advert-image" data-image-src="assets/images/content/ads.jpg">
              <div class="ads-overlay"></div>
              <span>- 1918 -</span>
              <h3><a href="javascript:void(0)">This house is over a century old.</a></h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida.</p>
              <div class="button-section">
                <a href="javascript:void(0)" class="btn btn-o-primary btn-sm">View More</a>
              </div>
            </div>
          </div>
        </aside>
      </div>
    </div>
  </div>
</section>
<!-- END PROPERTIES  -->
<?php get_footer(); ?>
