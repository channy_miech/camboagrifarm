    <?php
get_header();
/*
  Template Name: Properties Template
 */
?>
<!-- BEGIN HEADER BACKGROUND -->
 <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

<div class="header-background-box half">
    <div class="header-background"   style="background: url('<?php echo $backgroundImg[0]; no-repeat; ?>')">
        <div cstyle="background: url('<?php echo $backgroundImg[0]; no-repeat; ?>')"lass="webkit-and-moz-overlay-background">
            <div class="container">
                <div class="center-section">
                    <h2 class="section-title-border whiteTitleBorder"><?php the_title(); ?></h2>
                    <?php  while ( have_posts() ) : the_post(); ?> 
                    <div class="section-subtitle"> <?php the_content(); ?></div>
                    <?php
                         endwhile; //resetting the page loop
                         wp_reset_query(); //resetting the page query
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER BACKGROUND -->

<!-- BEGIN SITE MAP -->
<div class="site-map">
    <div class="container">
        <a href="<?php echo esc_url(home_url('/')); ?>">Home</a>
        <a href="<?php echo esc_url(home_url('/')); ?><?php the_title(); ?>"><?php the_title(); ?></a>
    </div>
</div>
<!-- END SITE MAP -->

<!-- BEGIN PROPERTIES  -->
<section class="properties-items">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="sorting-bar clearfix">
                    <div class="col-md-3 col-sm-3 form-sort-control">
                        <select name="sort-by" class="cs-select form-control show-tick selectpicker">
                            <option>Most Recent</option>
                            <option>Price High to Low</option>
                            <option>Price Low to High</option>
                        </select>
                    </div>
                    <div class="view-mode">
                        <span>View Mode:</span>
                        <ul>
                            <li class="active" id="p-showTiles"><i class="fa fa-th"></i></li>
                            <li id="p-showList"><i class="fa fa-th-list"></i></li>
                        </ul>
                    </div>
                </div>
                <div id="property-listing" class="flex-row row grid-style">
                    <div class="item col-md-4"><!-- On item  -->
                        <div class="item-box">
                            <label class="decor-label to-let">To Let</label>
                            <a href="property-single.html">
                                <div class="main-content">
                                    <div class="image-box">
                                        <img src="http://placehold.it/700x516&text=IMAGE+PLACEHOLDER" alt="">
                                        <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                    </div>
                                    <div class="main-info">
                                        <div class="property-tilte">
                                            Spacious 3 Bedroom Semi-Detached for sale
                                        </div>
                                        <div class="property-price">
                                            <span>$800</span>
                                        </div>
                                        <hr>
                                        <div class="property-desc">
                                            Beautiful apartment in a great, very calm and safe place.
                                        </div>
                                        <div class="property-category">
                                            <span class="decor"></span>
                                            <span class="title">Our Tree Villas</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="property-bottom-info">
                                <div class="icons">
                                    <span>
                                        <i class="fa fa-bed"></i> 3
                                    </span>
                                    <span>
                                        <i class="fa fa-bath" aria-hidden="true"></i> 2
                                    </span>
                                </div>
                                <div class="text">
                                    <span>350 sq ft</span>
                                    <span><i class="fa fa-heart-o"></i></span>
                                </div>
                            </div>
                        </div>
                    </div><!-- OFF item  -->
                    
                   
                    
                </div>
                <div class="clearfix"></div>
                <div class="pagination">
                    <ul id="previous">
                        <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                    </ul>
                    <ul>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                    </ul>
                    <ul id="next">
                        <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                </div>
            </div><!-- OFF col-md-9 -->

            <div class="col-md-3 col-sm-4">
                <aside>
                    <!-- BEGIN SEARCH FILTER  -->
                    <div class="edit-marginBottom30 edit-marginTop60">
                        <form action="#" class="form-action form-dark">
                            <div class="form-title-section text-center">
                                <div class="form-title-display">
                                    <div class="form-title text-center">
                                        <span class="text-uppercase">Search for property</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row box-action-one">
                                    <div class="col-sm-12 cs-padding">
                                        <label>Location</label>
                                        <input type="text" class="form-control" placeholder="Streed, town, city">
                                    </div>
                                    <div class="col-sm-12 cs-padding">
                                        <label>Property Status</label>
                                        <select name="status type" class="cs-select form-control show-tick selectpicker">
                                            <option selected>Any</option>
                                            <option>Rent</option>
                                            <option>Buy</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 cs-padding">
                                        <label>Property type</label>
                                        <select name="propery type" class="cs-select form-control show-tick selectpicker">
                                            <option selected>Type</option>
                                            <option>Villa</option>
                                            <option>Family House</option>
                                            <option>Single Home</option>
                                            <option>Cottage</option>
                                            <option>Apartment</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 cs-padding">
                                        <!-- Date field !-->
                                        <label>Date range</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control" placeholder="Date" id="dp-date">
                                        </div>
                                    </div>
                                    <div class="col-lg-6  cs-padding">
                                        <label>Min Beds</label>
                                        <select name="beds" class="cs-select form-control show-tick selectpicker">
                                            <option>Any</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 cs-padding">
                                        <label>Min Baths</label>
                                        <select name="beds" class="cs-select form-control show-tick selectpicker">
                                            <option>Any</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 cs-padding">
                                        <label>Min Price</label>
                                        <select name="beds" class="cs-select form-control show-tick selectpicker">
                                            <option>Any</option>
                                            <option>$1000</option>
                                            <option>$5000</option>
                                            <option>$10000</option>
                                            <option>$50000</option>
                                            <option>$100000</option>
                                            <option>$500000</option>
                                            <option>$1000000</option>
                                            <option>$3000000</option>
                                            <option>$5000000</option>
                                            <option>$10000000</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 cs-padding">
                                        <label>Max Price</label>
                                        <select name="baths" class="cs-select form-control show-tick selectpicker">
                                            <option>Any</option>
                                            <option>$1000</option>
                                            <option>$5000</option>
                                            <option>$10000</option>
                                            <option>$50000</option>
                                            <option>$100000</option>
                                            <option>$500000</option>
                                            <option>$1000000</option>
                                            <option>$3000000</option>
                                            <option>$5000000</option>
                                            <option>$10000000</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 cs-padding">
                                        <label>Min Area (Sq Ft)</label>
                                        <input name="min-area" type="text" class="form-control" placeholder="Any">
                                    </div>
                                    <div class="col-sm-12 cs-padding">
                                        <label>Max Area (Sq Ft)</label>
                                        <input name="max-area" type="text" class="form-control" placeholder="Any">
                                    </div>
                                    <div class="col-sm-12 cs-padding">
                                        <button type="submit" class="btn btn-primary cs-search-btn btn-sm"><i class="fa fa-search"></i> Search Now</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END SEARCH FILTER  -->

                    <div class="sb-latest-posts sb-featured edit-margin0">
                        <h4 class="widget-title">Recent Properties</h4>
                        <ul>
                            <li><a href="#">
                                    <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                                    <div class="text-box">
                                        <span class="title">Spacious 9 Bedroom villa for sale</span>
                                        <div class="property-price">
                                            <span class="old-price">$950,000</span>
                                            <span>$850,000</span>
                                        </div>
                                    </div>
                                </a></li>

                            <li><a href="#">
                                    <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                                    <div class="text-box">
                                        <span class="title">Spacious 8 Bedroom villa for sale</span>
                                        <div class="property-price">
                                            <span>$800,000</span>
                                        </div>
                                    </div>
                                </a></li>
                        </ul>
                    </div>

                    <div class="sb-latest-posts edit-margin0">
                        <h4 class="widget-title">Latest News</h4>
                        <ul>
                            <li><a href="#">
                                    <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                                    <div class="text-box">
                                        <span class="title">Creamy Checken Pesto Pasta</span>
                                        <span class="date">December 6, 2016</span>
                                    </div>
                                </a></li>

                            <li><a href="#">
                                    <img src="http://placehold.it/160x160&text=IMAGE+PLACEHOLDER" alt="" class="image">
                                    <div class="text-box">
                                        <span class="title">Top 15 Best Restaurants</span>
                                        <span class="date">December 6, 2016</span>
                                    </div>
                                </a></li>
                        </ul>
                    </div>

                    <div class="sb-advert edit-marginTop20">
                        <div class="advert-image" data-image-src="assets/images/content/ads.jpg">
                            <div class="ads-overlay"></div>
                            <span>- 1918 -</span>
                            <h3><a href="javascript:void(0)">This house is over a century old.</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida.</p>
                            <div class="button-section">
                                <a href="javascript:void(0)" class="btn btn-o-primary btn-sm">View More</a>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>
<!-- END PROPERTIES  -->

<?php get_footer(); ?>

