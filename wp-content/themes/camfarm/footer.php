
<script type='text/javascript'>
(function(){ var widget_id = 'bytbbf057C';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id;
var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!--BEGIN FOOTER -->
	<footer id="footer" class="footer-background">
		<div id="footer-top" class="container">
			<div class="row">
                            <div class="col-md-3 col-3-footer"><?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
                               <ul class="socials-box">
															 <li><a href="<?php echo get_theme_mod('top_bar_social_1_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_1_icon'); ?> fa-2"></i></a></li>
															 <li><a href="<?php echo get_theme_mod('top_bar_social_2_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_2_icon'); ?> fa-2"></i></a></li>
															 <li><a href="<?php echo get_theme_mod('top_bar_social_3_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_3_icon'); ?> fa-2"></i></a></li>
															 <li><a href="<?php echo get_theme_mod('top_bar_social_4_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_4_icon'); ?> fa-2"></i></a></li>
															 <li><a href="<?php echo get_theme_mod('top_bar_social_5_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_5_icon'); ?> fa-2"></i></a></li>
                               </ul>
                            </div>
                            <div class="col-md-3 col-3-footer" style="padding-left: 89px;"><?php dynamic_sidebar( 'second-footer-widget-area' ); ?></div>

                            <div class="col-md-3 col-3-footer"><?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?></div>
			</div>
		<div class="row">
			<div class="col-md-12">
			<div class="logo-cards">
				<p>Choose Language</p>
					<div class="lang-logo">
					<?php
							qtranxf_generateLanguageSelectCode(array(
							'type'   => 'image'
							))
					?>
					</div>
					</div>
				<hr>
				<div class="logo-cards">
					<p>We Accept Debit & Credit Cards</p>
						<?php
								$args = array('post_type' => 'post', 'posts_per_page' => 10, 'category_name'=>'Card');
								 $loop = new WP_Query($args);
								 while ($loop->have_posts()) : $loop->the_post();
			 			?>
						<div class="item">
							<?php if (has_post_thumbnail()) : ?>
									<?php the_post_thumbnail(); ?>
							<?php endif; ?>
						</div>
					<?php endwhile ; ?>
			</div>
		</div>
		</div>

		<!--BEGIN COPYRIGHT -->
		<div id="copyright">
			<a href="#" class="scrollTopButton">
         <span class="button-square">
             <i class="fa fa-angle-double-up"></i>
         </span>
      </a>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<span class="allright">2017 <strong class="t-color">Camboagrifarm </strong> All rights reserved. Designed by <strong class="t-color"> Nexuslab Developer</strong></span>
					</div>
				</div>
			</div>
		</div>
		<!-- END COPYRIGHT -->
	</footer>
	<!-- END FOOTER -->
	</div>
	<!-- END WRAPPER -->
	<!-- Libs -->
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/common/jquery-1.x-git.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/common.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/bootstrap-select.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/bootstrap-progressbar.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/isotope.pkgd.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.min.js"></script>

	<!-- Datepiker Script -->
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/datepiker/js/moment.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/datepiker/js/bootstrap-datepicker.js"></script>
	<!-- Hero Slide Scripts -->
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/xpro/js/slider.js"></script>
	<!-- Template Scripts -->
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/scripts.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/contact-mail.js"></script>

	<script type="text/javascript">
		// Hero Slide// --------------------------
		jQuery(document).ready(
		    function() {
		        var slider = new XPRO.Controls.Slider(null);
		        slider.initSlider("scroller", {
		                "mode"              : "fade",
		                "dir"               : "left",
		                "iniWidth"          : 1200,
		                "iniHeight"         : 460,
		                "autoRun"           : true,
		                "interval"          : 6000,
		                "autoHeightMode"    : "maintainratio",
		                "thumbnails"        : true,
		                "stopOnHover"       : false,
		                "imageVAlign"       : "center",
		                "showProgress"      : false,
		                "enableNavigation"  : true,
		             });

		        jQuery(".xp-custom-navigation").on("click", function() {
		            slider.forward();
		            return false;
		        });
		    }
			);


	</script>
        <?php wp_footer(); ?>
	</body>
	</html>
