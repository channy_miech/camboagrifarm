<?php get_header() ;
/*
  Template Name: land sale
 */
 ?>
<!-- BEGIN HEADER BACKGROUND -->
<?php $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
<div class="header-background-box half">
    <div class="header-background"   style="background: url('<?php echo $backgroundImg[0];
no - repeat; ?>')" >
        <div class="webkit-and-moz-overlay-background">
            <div class="container">
                <div class="center-section">
                    <h2 class="section-title-border whiteTitleBorder"><?php the_title(); ?></h2>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="section-subtitle"> <?php the_content(); ?></div>
                        <?php
                    endwhile; //resetting the page loop
                    wp_reset_query(); //resetting the page query
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER BACKGROUND -->

<!-- BEGIN SITE MAP -->
<div class="site-map">
    <div class="container">
        <a href="<?php echo esc_url(home_url('/')); ?>">Home</a>
        <a href="<?php echo esc_url(home_url('/?page_id=317')); ?>"><?php the_title(); ?></a>
    </div>
</div>
<!-- END SITE MAP -->

<!-- BEGIN PROPERTIES  -->
<section class="properties-items">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-8">
        <div class="sorting-bar clearfix">
          <div class="col-md-3 col-sm-3 form-sort-control">
            <h4 >Feature Land <h4>
          </div>
          <div class="view-mode">
            <span>View Mode:</span>
            <ul>
              <li class="active" id="p-showTiles"><i class="fa fa-th"></i></li>
              <li id="p-showList"  ><i class="fa fa-th-list"></i></li>
            </ul>
          </div>
        </div>
        <div id="property-listing" class="flex-row row grid-style">
          <?php
                $args = array('post_type' => 'land-and-farm', 'posts_per_page' => 10, 'category_name'=>'land-sale');
                $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();
         ?>
          <div class="item col-md-4"><!-- On item  -->
            <div class="item-box">
              <label class="decor-label to-let"><?php  echo get_post_meta($post->ID, 'wpcf-status-item', true); ?></label>
                <a href="<?php the_permalink(); ?>">
                  <div class="main-content">
                      <div class="image-box">
                        <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-home-image', true); ?>"  style="width:100%;height:200px;">

                         <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                      </div>
                      <div class="main-info">
                          <div class="property-tilte">
                            <?php echo mb_strimwidth(get_the_title(), 0, 25, "..."); ?>
                          </div>
                          <div class="property-price">
                              <span> <?php  echo get_post_meta($post->ID, 'wpcf-item-price', true); ?> $</span>
                            <!-- <span class="pull-right"><p><?php // echo get_post_meta($post->ID, 'wpcf-address', true); ?></p></span> -->
                          </div>
                          <hr>
                          <div class="property-desc">
                            <p>Address: <?php echo mb_strimwidth(get_post_meta($post->ID, 'wpcf-address', true), 0, 20, "..."); ?></p>
                            <p><?php echo mb_strimwidth(get_the_content(), 0, 100, "..."); ?></p>
                          </div>
                          <hr>

                      </div>
                  </div>
                </a>
                <div class="property-bottom-info">
                    <div class="text">
                    </div>
                </div>
            </div>
          </div><!-- OFF item  -->
        <?php endwhile ; ?>

        <!-- feature farm -->
        <?php
              $args = array('post_type' => 'land-and-farm', 'posts_per_page' => 10, 'category_name'=>'land and farm');
              $loop = new WP_Query($args);
              while ($loop->have_posts()) : $loop->the_post();
       ?>
        <div class="item col-md-4"><!-- On item  -->
          <div class="item-box">
            <label class="decor-label to-let"><?php  echo get_post_meta($post->ID, 'wpcf-status-item', true); ?></label>
              <a href="<?php the_permalink(); ?>">
                <div class="main-content">
                    <div class="image-box">
                      <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-home-image', true); ?>"  style="width:100%;height:200px;">

                       <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                    </div>
                    <div class="main-info">
                        <div class="property-tilte">
                          <?php echo mb_strimwidth(get_the_title(), 0, 25, "..."); ?>
                        </div>
                        <div class="property-price">
                            <span> <?php  echo get_post_meta($post->ID, 'wpcf-item-price', true); ?> $</span>
                          <!-- <span class="pull-right"><p><?php // echo get_post_meta($post->ID, 'wpcf-address', true); ?></p></span> -->
                        </div>
                        <hr>
                        <div class="property-desc">
                          <p>Address: <?php echo mb_strimwidth(get_post_meta($post->ID, 'wpcf-address', true), 0, 20, "..."); ?></p>
                          <p><?php echo mb_strimwidth(get_the_content(), 0, 100, "..."); ?></p>
                        </div>
                        <hr>

                    </div>
                </div>
              </a>
              <div class="property-bottom-info">
                  <div class="text">
                  </div>
              </div>
          </div>
        </div><!-- OFF item  -->
      <?php endwhile ; ?>
        <!-- end feature of farm -->
        </div>
        <div class="clearfix"></div>

      </div><!-- OFF col-md-9 -->

      <div class="col-md-3 col-sm-4">
        <aside>
          <!-- BEGIN SEARCH FILTER  -->
          <div class="edit-marginBottom30 edit-marginTop60">
            <!-- include search form -->
            <?php include 'searchform.php'; ?>
          <!-- END SEARCH FILTER  -->
          <div class="sb-latest-posts edit-margin0">
            <h4 class="widget-title">Latest News</h4>
            <ul>
              <?php
                 $args = array('post_type' => 'post', 'posts_per_page' => 5, 'category_name'=>'BLOG');
                 $loop = new WP_Query($args);
                 while ($loop->have_posts()) : $loop->the_post();
             ?>
              <li><a href="#">
                <?php if (has_post_thumbnail()) : ?>
                   <?php the_post_thumbnail(); ?>
                <?php endif; ?>
                <div class="text-box">
                  <span class="title"><?php the_title(); ?></span>
                  <span class="date"><?php echo date(get_option('date_format')); ?></span>
                </div>
              </a></li>
            <?php endwhile; ?>
            </ul>
          </div>

          <div class="sb-advert edit-marginTop20">
            <h4 class="widget-title" style="text-align: left;">Contact Agents</h4>
            <?php
            $args = array('post_type' => 'agents_experts', 'posts_per_page' => 10,'category_name' => 'Our Agents');
            $loop = new WP_Query($args);
             while ($loop->have_posts()) : $loop->the_post();
            ?>
            <div class="person-item owl-carousel owl-property-expert" >

              <div class="person-box " >
                <div class="person-image">
                  <a href="<?php the_permalink() ; ?>"><img src="  <?php  echo get_post_meta($post->ID, 'wpcf-image', true); ?>" ><div class="overlay"></div></a>
                  <ul class="socials-box">
                          <?php
                              $args = array(
                                'theme_location' => 'social-menu',
                                'container'   => 'nav',
                                'container_class' => 'socials',
                                'container_id' => 'socials',
                                'link_before' => '<span class="sr-text"  >',
                                'link_after' => '</span>'
                              );
                              wp_nav_menu($args);
                           ?>
                      </ul>
                </div>
                <div class="person-content">
                  <h4><a href="<?php the_permalink() ; ?>"><?php the_title() ; ?></a><small class="text-normal"><?php  echo get_post_meta($post->ID, 'wpcf-address', true); ?></small></h4>
                  <span class="small-title"></span>
                  <hr>
                  <ul class="person-contacts">
                    <li><i class="fa fa-phone"></i><?php  echo get_post_meta($post->ID, 'wpcf-fax-phone-number', true); ?></li>
                    <li><i class="fa fa-fax"></i><?php  echo get_post_meta($post->ID, 'wpcf-fax-phone-number', true); ?></li>
                    <li><i class="fa fa-envelope-o"></i> <a href="mailto:<?php  echo get_post_meta($post->ID, 'wpcf-email', true); ?>"><?php  echo get_post_meta($post->ID, 'wpcf-email', true); ?></a></li>
                  </ul>
                </div>

              </div>
            </div>
              <?php endwhile; ?>
          </div>
        </aside>
      </div>
    </div>
  </div>
</section>
<!-- END PROPERTIES  -->

<?php get_footer (); ?>
