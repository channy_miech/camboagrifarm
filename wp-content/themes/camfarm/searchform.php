<?php
/**
 * The template for displaying search forms in Shape
 *
 * @package Shape
 * @since Shape 1.0
 */
?>

    <form class="form-action form-dark" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
      <div class="form-title-section text-center">
        <div class="form-title-display">
            <div class="form-title text-center">
                <span class="text-uppercase">Search for land and farm</span>
            </div>
        </div>
      </div>
      <div class="form-group">
        	<div class="row box-action-one">
            <div class="col-sm-12 cs-padding">
              <label for="s" class="assistive-text"><?php _e( 'Title', 'shape' ); ?></label>
              <input type="hidden" value="6" name="cat[]" id="scat" />
              <input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Title &hellip;', 'shape' ); ?>" />
            </div>
            <div class="col-sm-12 cs-padding">
              <label for="s1" class="assistive-text"><?php _e( 'Location', 'shape' ); ?></label>
              <input type="text" class="field" name="s1" value="<?php echo esc_attr( get_search_query() ); ?>" id="s1" placeholder="<?php esc_attr_e( 'Location &hellip;', 'shape' ); ?>" />
            </div>
            <div class="col-sm-12 cs-padding">
              <input type="submit"  class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'shape' ); ?>" />
            </div>
        </div>
      </div>
    </form>

<script type='text/javascript'>

</script>
