<?php
get_header();
/*
    Template Name: Contact_us
*/
?>
    <!-- BEGIN HEADER BACKGROUND --> 
		<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
		<?php $backgroundImg = $backgroundImg[0];?>
		<div class="header-background-box half">
		<div class="header-background" style="background-image:url(<?php echo $backgroundImg ?>);">
		    <div class="webkit-and-moz-overlay-background">
		        <div class="container">
		            <div class="center-section">
		                <h2 class="section-title-border whiteTitleBorder"><?php the_title(); ?></h2>
		                <?php  while ( have_posts() ) : the_post(); ?>
	                    <div class="section-subtitle"> <?php the_content(); ?></div>
	                    <?php
	                         endwhile; //resetting the page loop
	                         wp_reset_query(); //resetting the page query
	                    ?>
		            </div>
		        </div>
		    </div>
		</div>
		</div>
	<!-- END HEADER BACKGROUND -->

	<!-- BEGIN SITE MAP -->
    <div class="site-map">
        <div class="container">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a>
            <a href="<?php echo esc_url( home_url( '/contact-us' ) ); ?>"><?php the_title(); ?></a>
        </div>
    </div>
    <!-- END SITE MAP -->

    <!-- BEGIN CONTACT SECTION-->
		<section class="contact-section ">
			<div class="row">
				<div class="col-md-8 col-sm-7 edit-padding0">
						<div id="contactMap" class="contact-map">
							<?php
                                $args = array('post_type' => 'post', 'posts_per_page' => 10, 'category_name'=>'contact_map');
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                            ?>
	                            <article>
	                        		
	                                <?php the_content(); ?>
	         
	                            </article>
                        	<?php endwhile; ?>
							<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.0966810946647!2d104.90238631418839!3d11.544922047696263!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109511d4b43131b%3A0x12bc4cb69f62c090!2sKhmer+Soviet+Friendship+Hospital!5e0!3m2!1sen!2skh!4v1511148466775" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
						</div>
				</div>
					<div class="col-md-4 col-sm-5 contact-info-right ct-background-color edit-displayTable" data-bgcolor="#f4f4f4">
						<div class="contact-info">
								<h3>Get in Touch</h3>
								<p class="center-block textBox-width70">If you have any questions please don’t hesitate to get in touch using the form below</p>
							<form method="post" id="contactform" name="contactform" class="contact-form form-margin" action="assets/mail/contact.php">
								<div class="col-md-12">
									<div class="form-group"> <!-- Name field !-->
										<input type="text" id="name" name="name" class="form-control" placeholder="Your Name *" >
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group"> <!-- Email field !-->
										<input type="email" id="email" name="email" class="form-control" placeholder="Your Email *">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group"> <!-- Phone field !-->
										<input type="number" class="form-control" id="phone" name="phone" placeholder="Your Phone">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group"> <!-- Message field !-->
										<textarea cols="6" rows="5" class="form-control" id="comments" name="comments" placeholder="Your Message"></textarea>
									</div>
								</div>
								<div class="col-md-12">
								 <!-- Submit button !-->
								 <div class="center-position">
									<input id="submit" type="submit" class="btn btn-primary btn-sm center-button edit-marginTop10" value="Send Message">
									</div>
								</div>
							</form>
							<div id="message"></div>
						</div>
					</div>
				</div>
				<div class="row ct-background-color" data-bgcolor="#ffffff" data-bordertop="1px solid #e2e2e2">
					<div class="container edit-paddingBoth40">

						<div class="col-md-3 col-sm-6">
							<div class="iconBox">
								<div class="iconBox-icon center-block edit-marginBottom30"><i class="fa  fa-map-marker"></i></div>
								<div class="iconBox-description">
									<span class="iconBox-title edit-marginBottom10">Visit Us</span>
									<span class="iconBox-text">
										1234 Whimsy Lane, City 8878
										<br>
										United States, Chalfont, 18814
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="iconBox">
								<div class="iconBox-icon center-block edit-marginBottom30"><i class="fa fa-phone"></i></div>
								<div class="iconBox-description">
									<span class="iconBox-title edit-marginBottom10">Call Us</span>
									<span class="iconBox-text">
										Tell: (+001) 888 88 8888
										<br>
										Fax: (+001) 888 88 8888
							 		</span>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="iconBox">
								<div class="iconBox-icon center-block edit-marginBottom30"><i class="fa fa-envelope"></i></div>
								<div class="iconBox-description">
									<span class="iconBox-title edit-marginBottom10">Email Us</span>
										<span class="iconBox-text">
											<a href="mailto:office@youremail.com">office@youremail.com</a>
											<br>
											<a href="mailto:sales@youremail.com">sales@youremail.com</a>
								 		</span>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="iconBox">
								<div class="iconBox-icon center-block edit-marginBottom30"><i class="fa fa-share-alt"></i></div>
								<div class="iconBox-description">
									<span class="iconBox-title edit-marginBottom10">Connect With Us</span>
										<ul class="socials-box">
											<li><a href="#"><i class="fa  fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
								</div>
							</div>
						</div>
				</div>
			</div>
		</section>

<section class="container categories">
	<h2 class="text-center">Categories</h2>
	<?php  $categories = get_categories(); ?>
		
	<div class="row">
		<?php foreach($categories as $category): ?>
			<div class="col-xs-6 col-md-6">
				<a href="<?php echo $var ?>">
					<?php echo $category->name; ?>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
</section>
	<!-- </section> -->
		<!-- END CONTACT SECTION -->

  	<!-- BEGIN BLOG PAGE  -->
            <section class="blog-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                                $args = array('post_type' => 'post', 'posts_per_page' => 10, 'category_name'=>'contact_us');
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                            ?>
	                            <article>
	                        		<h3 style="text-decoration: underline;"><?php the_title(); ?></h3>
	                                <?php the_content(); ?>
	                                <?php the_post_thumbnail(); ?>
	                            </article>
                        	<?php endwhile; ?> 
  
                        </div>
                    </div>
                </div> <!-- END container  -->
            </section>
    <!-- END BLOG PAGE -->

<?php get_footer(); ?>
            