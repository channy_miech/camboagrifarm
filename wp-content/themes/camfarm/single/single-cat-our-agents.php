<?php get_header(); ?>
<!-- BEGIN PERSON DETAIL  -->
<section>
  <div class="container">
    <h2 class="section-title-border">Our Agent</h2>
    <div class="section-subtitle">
      <!-- The internet is changing the way the property industry works. -->
    </div>
    <?php
         $args = array('post_type' => 'agents_experts', 'posts_per_page' => 10,'category_name' => 'Our Agents');
         $loop = new WP_Query($args);
          while ($loop->have_posts()) : $loop->the_post();
   ?>
    <div class="row edit-marginTop30">
      <div class="col-sm-9 col-sm-8">
        <div class="person-detail">
          <div class="person-item">
            <div class="person-box">
              <div class="person-image">
                <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-image', true); ?>" >

               <!-- socila media menu -->
                <ul class="socials-box">
                  <li><a href="<?php echo get_theme_mod('top_bar_social_1_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_1_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_2_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_2_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_3_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_3_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_4_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_4_icon'); ?> fa-2"></i></a></li>
                  <li><a href="<?php echo get_theme_mod('top_bar_social_5_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_5_icon'); ?> fa-2"></i></a></li>
                    </ul>
              </div>
              <div class="person-content">
                <h4>Thomas Albert <small class="text-normal"><?php  echo get_post_meta($post->ID, 'wpcf-address', true); ?></small>
                </h4>
                <ul>
                  <li>
                    <span class="small-title"><i class="fa fa-handshake-o"></i> <?php  echo get_post_meta($post->ID, 'wpcf-assigned-properties', true); ?> Assigned Properties</span>
                  </li>
                </ul>
                <hr>
                <ul class="person-contacts">
                  <li><i class="fa fa-phone"></i>+001 800 88 88</li>
                  <li><i class="fa fa-fax"></i>+001 800 88 88</li>
                  <li><i class="fa fa-envelope-o"></i> <?php  echo get_post_meta($post->ID, 'wpcf-email', true); ?></li>
                </ul>
              </div>
            </div>
          </div><!-- person-item -->
        </div>
        <div class="person-description">
          <div class="col-md-12">
            <p><?php the_content() ; ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4">
        <aside>
          <div class="person-progress-boxes">
            <h4 class="widget-title">Agent Skills</h4>

            <span class="small-title">Expensive Property</span>
            <div class="progress progress-e">
              <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="<?php  echo get_post_meta($post->ID, 'wpcf-expensive-property', true); ?>"></div>
            </div>
            <span class="small-title">Relatively cheap Properties</span>
            <div class="progress progress-e">
              <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="<?php  echo get_post_meta($post->ID, 'wpcf-relatively-cheap-properties', true); ?>"></div>
            </div>
            <span class="small-title">Cheap valuable rental properties</span>
            <div class="progress progress-e">
              <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="<?php  echo get_post_meta($post->ID, 'wpcf-cheap-valuable-rental-properties', true); ?>"></div>
            </div>
          </div>
          <div class="person-contact">
            <a class="btn btn-primary btn-sm" href="#">Contact Agent</a>
          </div>
        </aside>
      </div>
    </div> <!-- row -->
  <?php endwhile; ?>
  </div>
</section>
<!-- END PERSON DETAIL -->
<!-- PERSONS TEAM  -->
<section class="persons-section edit-padding0 edit-paddingTop10">
    <div class="container">
        <h4 class="persons-section-title">Our Agents</h4>
    </div>
    <div class="persons-section edit-paddingBottom10 overflow-visible ct-background-color" data-bgcolor="#fbfbfb" data-borderTop="1px solid #e2e2e2">
        <div class="container">
            <div class="owl-carousel owl-persons edit-paddingBoth40 owl-top-nav">

                 <?php
                      $args = array('post_type' => 'agents_experts', 'posts_per_page' => 10,'category_name' => 'Our Agents');
                      $loop = new WP_Query($args);
                       while ($loop->have_posts()) : $loop->the_post();
                ?>

                <div class="item"><!-- On item -->
                    <div class="person-item">
                        <div class="person-box">
                            <div class="person-image">
                                  <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-image', true); ?>" >
                                <ul class="socials-box">
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_1_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_1_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_2_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_2_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_3_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_3_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_4_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_4_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_5_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_5_icon'); ?> fa-2"></i></a></li>
                                    </ul>
                            </div>

                            <div class="person-content">
                                <h4><a href="<?php the_permalink() ; ?>"><?php the_title() ; ?></a><small class="text-normal"><?php  echo get_post_meta($post->ID, 'wpcf-address', true); ?></small></h4>
                                <span class="small-title"></span>
                                <hr>
                                <ul class="person-contacts">
                                    <li><i class="fa fa-phone"></i><?php  echo get_post_meta($post->ID, 'wpcf-fax-phone-number', true); ?></li>
                                    <li><i class="fa fa-fax"></i><?php  echo get_post_meta($post->ID, 'wpcf-phone-number', true); ?></li>
                                    <li><i class="fa fa-envelope-o"></i><?php  echo get_post_meta($post->ID, 'wpcf-email', true); ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Off item -->
                <?php endwhile; ?>
            </div><!-- OFF Owl Carousel  -->
        </div>
    </div>
</section>
<!-- END PERSONS TEAM -->
<?php get_footer() ; ?>
