<?php get_header(); ?>



<!-- BEGIN BLOG PAGE  -->
<section class="blog-content" >
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <article>
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail(); ?>
                            <?php endif; ?>
                            <div class="articleTop-info">
                                <span><i class="fa fa-calendar"></i> <?php echo date(get_option('date_format')); ?></span> <span><i class="fa fa-comments-o"></i> <?php printf(_nx('One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain'), number_format_i18n(get_comments_number())); ?></span>
                                <div class="right-position">
                                    <span class="post-author">by <a href="#"><?php the_author(); ?> </a></span>

                                </div>
                            </div>
                            <a href=""><h4 class="post-title"><?php the_title(); ?></h4></a>
                            <div class="content-desc">
                                <?php the_content(); ?>
                            </div>


                            <div class="content-share">
                                <hr>
                                <div class="left-position">
                                    <h4 class="share-title">Share this Post:</h4>
                                    <ul class="socials-box">
                                        <li><a href="#"><div class="social-circle-border"><i class="fa  fa-facebook"></i></div></a></li>
                                        <li><a href="#"><div class="social-circle-border"><i class="fa fa-twitter"></i></div></a></li>
                                        <li><a href="#"><div class="social-circle-border"><i class="fa fa-google-plus"></i></div></a></li>
                                    </ul>
                                </div>
                                <a class="print-button" href="javascript:window.print();">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
                        </article>
                    <?php endwhile;
                endif; ?>

                <div class="clearfix"></div>
                <div>
                    <?php comments_template(); ?>
                </div>
            </div>
            <aside>
              <div class="col-md-3">
                  <form class="form-action form-dark" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
                    <div class="form-title-section text-center">
                      <div class="form-title-display">
                          <div class="form-title text-center">
                              <span class="text-uppercase">Search for land and farm</span>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      	<div class="row box-action-one">
                          <div class="col-sm-12 cs-padding">
                            <label for="s" class="assistive-text"><?php _e( 'Title', 'shape' ); ?></label>
                            <input type="hidden" value="6" name="cat" id="scat" />
                            <input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Title &hellip;', 'shape' ); ?>" />
                          </div>
                          <div class="col-sm-12 cs-padding">
                            <label for="s1" class="assistive-text"><?php _e( 'Location', 'shape' ); ?></label>
                            <input type="text" class="field" name="s1" value="<?php echo esc_attr( get_search_query() ); ?>" id="s1" placeholder="<?php esc_attr_e( 'Location &hellip;', 'shape' ); ?>" />
                          </div>
                          <div class="col-sm-12 cs-padding">
                            <input type="submit"  class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'shape' ); ?>" style="background:#53a5ec;color:#fff;width: 97%;"/>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="col-md-3" style="margin-top: -22px;margin-bottom: -30px;">
                <h4 class="widget-title">Feature</h4>
              </div>
            </aside>
                <?php
                      $args = array('post_type' => 'land-and-farm', 'posts_per_page' => 5, 'category_name'=>'land and farm');
                      $loop = new WP_Query($args);
                      while ($loop->have_posts()) : $loop->the_post();
               ?>

                <div class="item col-md-3"><!-- On item  -->
                  <div class="item-box">
                    <label class="decor-label to-let" ><?php  echo get_post_meta($post->ID, 'wpcf-status-item', true); ?></label>
                      <a href="<?php the_permalink(); ?>">
                        <div class="main-content">
                            <div class="image-box">
                              <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-home-image', true); ?>"  style="width:100%;height:150px;">

                               <div class="overlay-box"></div>
                            </div>
                            <div class="main-info">
                                <div class="property-tilte" style="margin-top:10px;">
                                  <?php echo mb_strimwidth(get_the_title(), 0, 25, "..."); ?>
                                </div>
                                <div class="property-price">
                                    <span> <?php  echo get_post_meta($post->ID, 'wpcf-item-price', true); ?> $</span>
                                  <!-- <span class="pull-right"><p><?php // echo get_post_meta($post->ID, 'wpcf-address', true); ?></p></span> -->
                                </div>

                                <div class="property-desc">
                                  <p>Address: <?php echo mb_strimwidth(get_post_meta($post->ID, 'wpcf-address', true), 0, 20, "..."); ?></p>
                                </div>
                                <hr>
                            </div>
                        </div>
                      </a>
                  </div>
                </div><!-- OFF item  -->
              <?php endwhile ; ?>
        </div><!-- END row  -->
    </div> <!-- END container  -->
</section>
<!-- END BLOG PAGE -->

<?php get_footer(); ?>
