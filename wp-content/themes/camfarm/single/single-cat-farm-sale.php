<?php get_header(); ?>

<!-- BEGIN HEADER BACKGROUND -->
<?php $backgroundImg = get_post_meta($post->ID, 'wpcf-header-image', true); ?>
<div class="header-background-box half">
    <div class="header-background"   style="background: url('<?php echo $backgroundImg;
no - repeat; ?>') ;height: 194px;" >
        <div class="webkit-and-moz-overlay-background">
            <div class="container">
                <div class="center-section">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER BACKGROUND -->
<!-- BEGIN SITE MAP -->
<div class="site-map">
    <div class="container">
        <a href="<?php echo esc_url(home_url('/')); ?>">Home</a>
        <a href="<?php echo esc_url(home_url('/?page_id=319')); ?>"><?php the_title(); ?></a>
    </div>
</div>
<!-- END SITE MAP -->
  <!-- BEGIN DETAIL PAGE  -->

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <section class="main-content">
      <div class="container">
        <div class="row">
          <div class="col-md-9 col-sm-8">
            <div class="product-cheader edit-marginBottom20 text-uppercase">
              <div class="product-topicon">
                <div class="col-md-7 edit-padding0">
                  <h4 class="product-title"><?php the_title() ; ?></h4>
                  <span class="text-small"><i class="fa fa-map-marker icon"></i> <?php  echo get_post_meta($post->ID, 'wpcf-address', true); ?></span>
                </div>
                <div class=" col-md-5 pull-right text-right edit-padding0">
                  <ul class="socials-box">
                    <li><a href="#"><div class="social-square-border icon-sm"><i class="fa  fa-print"></i></div></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="product-item-slide">
              <label class="decor-label to-let">
                <?php  echo get_post_meta($post->ID, 'wpcf-status-item', true); ?>
                <span><?php  echo get_post_meta($post->ID, 'wpcf-item-price', true); ?></span>
              </label>
              <div id="item-images" class="owl-carousel">
                <div class="item firstimage">
                  <img src="<?php  echo get_post_meta($post->ID, 'wpcf-home-image', true); ?>" alt="">
                </div>
              </div>
            </div>
            <div class="main-content edit-marginTop20">
            <div class="show-more edit-marginBottom20">
              <h4 class="edit-marginBottom30">Description</h4>
              <?php the_content() ; ?>
              <a href="#" class="show-more-button">Show More <i class="fa fa-angle-down"></i></a>
            </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-4">
            <aside>
              <!-- BEGIN SEARCH FILTER  -->
                <div class="edit-marginBottom30">
                  <form class="form-action form-dark" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
                    <div class="form-title-section text-center">
                      <div class="form-title-display">
                          <div class="form-title text-center">
                              <span class="text-uppercase">Search for land and farm</span>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      	<div class="row box-action-one">
                          <div class="col-sm-12 cs-padding">
                            <label for="s" class="assistive-text"><?php _e( 'Title', 'shape' ); ?></label>
                            <input type="hidden" value="6" name="cat" id="scat" />
                            <input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Title &hellip;', 'shape' ); ?>" />
                          </div>
                          <div class="col-sm-12 cs-padding">
                            <label for="s1" class="assistive-text"><?php _e( 'Location', 'shape' ); ?></label>
                            <input type="text" class="field" name="s1" value="<?php echo esc_attr( get_search_query() ); ?>" id="s1" placeholder="<?php esc_attr_e( 'Location &hellip;', 'shape' ); ?>" />
                          </div>
                          <div class="col-sm-12 cs-padding">
                            <input type="submit"  class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'shape' ); ?>" style="background:#53a5ec;color:#fff;width: 97%;"/>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <!-- END SEARCH FILTER  -->
              <div class="sb-latest-posts sb-featured edit-margin0">
                <h4 class="widget-title">News</h4>
                <ul>
                  <?php
                  $args = array('post_type' => 'post', 'posts_per_page' => 4, 'category_name'=>'BLOG');
                   $loop = new WP_Query($args);
                   while ($loop->have_posts()) : $loop->the_post();
                   ?>
                  <li><a href="<?php the_permalink() ; ?>">
                    <?php if (has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail(); ?>
                    <?php endif; ?>
                    <div class="text-box">
                      <span class="title"><?php the_title(); ?></span>
                      <div class="property-price">
                          <span><?php echo date(get_option('date_format')); ?></span>
                      </div>
                    </div>
                  </a></li>
                <?php endwhile ; ?>
                </ul>
              </div>
              <div class="sb-persons">
                <h4 class="widget-title">Contact Agent</h4>
                <?php
                $args = array('post_type' => 'agents_experts', 'posts_per_page' => 10,'category_name' => 'Our Agents');
                $loop = new WP_Query($args);
                 while ($loop->have_posts()) : $loop->the_post();
                ?>
                <div class="person-item owl-carousel owl-property-expert" >

                  <div class="person-box " >
                    <div class="person-image">
                      <a href="<?php the_permalink() ; ?>"><img src="  <?php  echo get_post_meta($post->ID, 'wpcf-image', true); ?>" ><div class="overlay"></div></a>
                      <ul class="socials-box">
                        <li><a href="<?php echo get_theme_mod('top_bar_social_1_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_1_icon'); ?> fa-2"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('top_bar_social_2_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_2_icon'); ?> fa-2"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('top_bar_social_3_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_3_icon'); ?> fa-2"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('top_bar_social_4_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_4_icon'); ?> fa-2"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('top_bar_social_5_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_5_icon'); ?> fa-2"></i></a></li>
                          </ul>
                    </div>
                    <div class="person-content">
                      <h4><a href="<?php the_permalink() ; ?>"><?php the_title() ; ?></a><small class="text-normal"><?php  echo get_post_meta($post->ID, 'wpcf-address', true); ?></small></h4>
                      <span class="small-title"></span>
                      <hr>
                      <ul class="person-contacts">
                        <li><i class="fa fa-phone"></i><?php  echo get_post_meta($post->ID, 'wpcf-fax-phone-number', true); ?></li>
                        <li><i class="fa fa-fax"></i><?php  echo get_post_meta($post->ID, 'wpcf-fax-phone-number', true); ?></li>
                        <li><i class="fa fa-envelope-o"></i> <a href="mailto:<?php  echo get_post_meta($post->ID, 'wpcf-email', true); ?>"><?php  echo get_post_meta($post->ID, 'wpcf-email', true); ?></a></li>
                      </ul>
                    </div>

                  </div>
                </div>

                  <?php endwhile; ?>
              </div>

            </aside>
          </div> <!-- END col  -->
        </div> <!-- END row  -->
      </div> <!-- END container  -->
  </section>
  <!-- END DETAIL PAGE -->
<?php endwhile;
endif; ?>



<?php get_footer(); ?>
