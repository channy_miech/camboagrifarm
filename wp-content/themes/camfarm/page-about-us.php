<?php
get_header();
/*
  Template Name: About_us
 */
?>
<!-- BEGIN HEADER BACKGROUND -->
<?php $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
<div class="header-background-box half">
    <div class="header-background"   style="background: url('<?php echo $backgroundImg[0];
no - repeat; ?>')" >
        <div class="webkit-and-moz-overlay-background">
            <div class="container">
                <div class="center-section">
                    <h2 class="section-title-border whiteTitleBorder"><?php the_title(); ?></h2>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="section-subtitle"> <?php the_content(); ?></div>
                        <?php
                    endwhile; //resetting the page loop
                    wp_reset_query(); //resetting the page query
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER BACKGROUND -->

<!-- BEGIN SITE MAP -->
<div class="site-map">
    <div class="container">
        <a href="<?php echo esc_url(home_url('/')); ?>">Home</a>
        <a href="<?php echo esc_url(home_url('/?page_id=6')); ?>"><?php the_title(); ?></a>
    </div>
</div>
<!-- END SITE MAP -->

<!-- BEGIN ABOUT US -->
<section>
    <div class="container">

         <?php
               $args = array('post_type' => 'post', 'posts_per_page' => 10, 'category_name'=>'ABOUT US');
               $loop = new WP_Query($args);
               while ($loop->have_posts()) : $loop->the_post();
        ?>
        <div class="row">
            <div class="col-md-5">
                 <?php if (has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail(); ?>
                 <?php endif; ?>
            </div><!-- OFF col-md-6 -->
            <div class="col-md-7">
                <div class="edit-paddingBoth20">
                    <h4><?php the_title() ; ?></h4>
                    <p><?php the_content() ; ?></p>
                </div>
            </div><!-- OFF col-md-6 -->
        </div>
        <?php endwhile; ?>
    </div>
</section>
<!-- END ABOUT US -->

<!-- PERSONS TEAM  -->
<section class="persons-section edit-padding0 edit-paddingTop10">
    <div class="container">
        <h3 class="section-title-border">Our Agents</h3>
    </div>
    <div class="persons-section edit-paddingBottom10 overflow-visible ct-background-color" data-bgcolor="#fbfbfb" data-borderTop="1px solid #e2e2e2">
        <div class="container">
            <div class="owl-carousel owl-persons edit-paddingBoth40 owl-top-nav">

                 <?php
                      $args = array('post_type' => 'agents_experts', 'posts_per_page' => 10,'category_name' => 'Our Agents');
                      $loop = new WP_Query($args);
                       while ($loop->have_posts()) : $loop->the_post();
                ?>

                <div class="item"><!-- On item -->
                    <div class="person-item">
                        <div class="person-box">
                            <div class="person-image">
                                <a href="<?php the_permalink() ; ?>">
                                       <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-image', true); ?>" >
                                    <div class="overlay"></div>
                                </a>
                                <!-- social metail menu -->
                                <ul class="socials-box">
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_1_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_1_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_2_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_2_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_3_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_3_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_4_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_4_icon'); ?> fa-2"></i></a></li>
                                  <li><a href="<?php echo get_theme_mod('top_bar_social_5_url'); ?>"><i class="<?php echo get_theme_mod('top_bar_social_5_icon'); ?> fa-2"></i></a></li>
                                    </ul>
                              <!-- end social metail menu -->
                            </div>
                            <div class="person-content">
                                <h4><a href="<?php the_permalink() ; ?>"><?php the_title() ; ?></a><small class="text-normal"><?php  echo get_post_meta($post->ID, 'wpcf-address', true); ?></small></h4>
                                <span class="small-title"></span>
                                <hr>
                                <ul class="person-contacts">
                                    <li><i class="fa fa-phone"></i><?php  echo get_post_meta($post->ID, 'wpcf-fax-phone-number', true); ?></li>
                                    <li><i class="fa fa-fax"></i><?php  echo get_post_meta($post->ID, 'wpcf-phone-number', true); ?></li>
                                    <li><i class="fa fa-envelope-o"></i><?php  echo get_post_meta($post->ID, 'wpcf-email', true); ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Off item -->
                <?php endwhile; ?>
            </div><!-- OFF Owl Carousel  -->
        </div>
    </div>
</section>
<!-- END PERSONS TEAM -->


<!-- BEGIN OUR PAERTNERS  -->
<section class="our-partners">
    <div class="container">
        <h3 class="section-title-border">Our Partners</h3>
        <div class="edit-marginTop40">
            <ul class="owl-carousel owl-ourPartners owl-center-nav">
                <?php
                       $args = array('post_type' => 'post', 'posts_per_page' => 10, 'category_name'=>'Our Partner');
                       $loop = new WP_Query($args);
                       while ($loop->have_posts()) : $loop->the_post();
                 ?>
                     <li>
                         <a href="#">
                         <?php if (has_post_thumbnail()) : ?>
                             <?php the_post_thumbnail(); ?>
                        <?php endif; ?>
                        </a>
                     </li>

                  <?php endwhile; ?>
            </ul>
        </div>
    </div>
</section>


<?php get_footer(); ?>
