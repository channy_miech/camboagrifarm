<?php
get_header();
/*
    Template Name: News Template
*/
?>
         <!-- BEGIN HEADER BACKGROUND -->
 <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

<div class="header-background-box half">
    <div class="header-background"   style="background: url('<?php echo $backgroundImg[0]; no-repeat; ?>')" >
        <div class="webkit-and-moz-overlay-background">
            <div class="container">
                <div class="center-section">
                    <h2 class="section-title-border whiteTitleBorder"><?php the_title(); ?></h2>
                    <?php  while ( have_posts() ) : the_post(); ?> ?>
                    <div class="section-subtitle"> <?php the_content(); ?></div>
                    <?php
                         endwhile; //resetting the page loop
                         wp_reset_query(); //resetting the page query
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER BACKGROUND -->

            <!-- BEGIN SITE MAP -->
            <div class="site-map">
                <div class="container">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_title(); ?>"><?php the_title(); ?></a>
                </div>
            </div>
            <!-- END SITE MAP -->

              <!-- BEGIN BLOG PAGE  -->
            <section class="blog-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                             <?php
                                $args = array('post_type' => 'post', 'posts_per_page' => 10, 'category_name'=>'news');
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                            ?>
                            <article>

                                  <a href="<?php the_permalink(); ?>">  <?php if (has_post_thumbnail()) : ?>
                                       <?php the_post_thumbnail(); ?>
                                    <?php endif; ?></a>
                                <div class="articleTop-info">
                                    <span><i class="fa fa-calendar"></i> <?php echo date(get_option('date_format')); ?></span> <span><i class="fa fa-comments-o"></i>
                                        <?php printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( get_comments_number() ) ); ?></span>
                                    <div class="right-position">
                                        <span class="post-author">by <a href="#"><?php the_author(); ?> </a></span>
<!--                                        <span class="post-category">in <a href="blog_sidebar.html">Blog</a>, <a href="blog_single.html">Wish House</a></span>-->
                                    </div>
                                </div>
                                <a href="<?php the_permalink() ; ?>"><h4 class="post-title"><?php the_title(); ?></h4></a>
                                <p><?php the_excerpt(); ?></p>
                            </article>
                        <?php endwhile; ?>

                            <div class="clearfix"></div>
                        </div>
                    <aside>
                      <div class="col-md-3">
                          <?php include 'searchform.php'; ?>
                      </div>
                      <div class="col-md-3" style="margin-top: -22px;margin-bottom: -30px;">
                        <h4 class="widget-title">Feature</h4>
                      </div>
                    </aside>
                        <?php
                              $args = array('post_type' => 'land-and-farm', 'posts_per_page' => 5, 'category_name'=>'land and farm');
                              $loop = new WP_Query($args);
                              while ($loop->have_posts()) : $loop->the_post();
                       ?>

                        <div class="item col-md-3"><!-- On item  -->
                          <div class="item-box">
                            <label class="decor-label to-let"><?php  echo get_post_meta($post->ID, 'wpcf-status-item', true); ?></label>
                              <a href="<?php the_permalink(); ?>">
                                <div class="main-content">
                                    <div class="image-box">
                                      <img src="  <?php  echo get_post_meta($post->ID, 'wpcf-home-image', true); ?>"  style="width:100%;height:150px;">

                                       <div class="overlay-box"></div>
                                    </div>
                                    <div class="main-info">
                                        <div class="property-tilte" style="margin-top:10px;">
                                          <?php echo mb_strimwidth(get_the_title(), 0, 25, "..."); ?>
                                        </div>
                                        <div class="property-price">
                                            <span> <?php  echo get_post_meta($post->ID, 'wpcf-item-price', true); ?> $</span>
                                          <!-- <span class="pull-right"><p><?php // echo get_post_meta($post->ID, 'wpcf-address', true); ?></p></span> -->
                                        </div>

                                        <div class="property-desc">
                                          <p>Address: <?php echo mb_strimwidth(get_post_meta($post->ID, 'wpcf-address', true), 0, 20, "..."); ?></p>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                              </a>
                          </div>
                        </div><!-- OFF item  -->
                      <?php endwhile ; ?>
                    </div>
                </div> <!-- END container  -->
            </section>
            <!-- END BLOG PAGE -->

<?php get_footer(); ?>
