<div id="pageloader">
  <div class="sk-cube-grid">
    <div class="sk-cube sk-cube1"></div>
    <div class="sk-cube sk-cube2"></div>
    <div class="sk-cube sk-cube3"></div>
    <div class="sk-cube sk-cube4"></div>
    <div class="sk-cube sk-cube5"></div>
    <div class="sk-cube sk-cube6"></div>
    <div class="sk-cube sk-cube7"></div>
    <div class="sk-cube sk-cube8"></div>
    <div class="sk-cube sk-cube9"></div>
  </div>
</div>

<!--  WRAPPER -->
        <div id="wrapper">
          <!-- Mobile menu overlay background -->
            <div class="page-overlay"></div>

            <!-- BEGIN HEADER -->
            <header id="header" class="header-default">
                <div id="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <ul id="top-info">
                                    <li><i class="fa  fa-phone fa-2"></i> 001-888-8888</li>
                                    <li><i class="fa  fa-envelope fa-2"></i> <a href="mailto:hello@wishhouse.com">hello@wishhouse.com</a></li>
                                    <li><a href="#"><i class="fa  fa-map-marker fa-2"></i> Location</a></li>
                                </ul>

                                <ul class="socials-box pull-right">
                                    <li><a href="#"><i class="fa  fa-facebook fa-2"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter fa-2"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus fa-2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container navbar-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3 header-left">
                                <a href="index.html" class="logo"><img src="<?php bloginfo('template_url') ?>/assets/images/logo.png" alt=""></a>
                            </div>
                            <div class="col-md-9 header-right">
                                <div class="call-center">
                                    <div><i class="fa  fa-phone fa-2"></i> Free Line For You
                                        <span>001-999 876 54</span></div>
                                </div>
                                <div class="header-search">
                                    <form class="search-form">
                                        <input type="text" placeholder="Search...">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                                <a class="btn btn-primary btn-success btn-sm" href="#">Free Valuation <i class="fa fa-chevron-right"></i></a>
                                <a class="btn btn-primary btn-sm" href="#">Book Viewing <i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="nav-mobile-btn is-closed animated fadeInLeft" data-toggle="offcanvas">
                    <span class="hamb-top"></span>
                    <span class="hamb-middle"></span>
                    <span class="hamb-bottom"></span>
                </button>

                <button class="search-mobile-btn s-is-closed animated fadeInRight" data-toggle="s-offcanvas">
                    <i class="fa fa-search"></i>
                </button>

                <div id="nav-section">
                    <div class="nav-background">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- BEGIN MAIN MENU -->
                                    <nav id="nav-mobile" class="navbar">
                                        <div class="special-actions">
                                            <a class="btn btn-primary" href="#"><i class="fa fa-plus"></i> Submit Propert</a>
                                        </div>
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown">
                                                <a class="active" href="index.html" data-toggle="dropdown" data-hover="dropdown">Home<i class="fa fa-angle-down"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li><a class="active" href="index.html">Home Image Slider</a></li>
                                                    <li><a href="index2.html">Home Video Slider</a></li>
                                                    <li><a href="index3.html">Home Onepage</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="about.html">About Us</a></li>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" data-hover="dropdown">Properties<i class="fa fa-angle-down"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="property-single.html">Properties Detail</a></li>
                                                    <li><a href="properties-grid.html">Properties Grid</a></li>
                                                    <li><a href="properties-list.html">Properties List</a></li>
                                                    <li><a href="properties-grid-map.html">Properties Grid Map</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" data-hover="dropdown">Blog<i class="fa fa-angle-down"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="blog_sidebar.html">Blog Right Sidebar</a></li>
                                                    <li><a href="blog_fullwidth.html">Blog Fullwidth</a></li>
                                                    <li><a href="blog_single.html">Blog Single</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" data-hover="dropdown">Pages<i class="fa fa-angle-down"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown-submenu">
                                                        <a href="#">Agents</a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="agent-detail.html">Agent Detail</a></li>
                                                            <li><a href="agent-listing.html">Agent Listing</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="shortcode.html">Shortcode</a></li>
                                                    <li><a href="gallery.html">Gallery</a></li>
                                                    <li><a href="gallery-thumb-overlay.html">Gallery Overlay</a></li>
                                                    <li><a href="pricing-tables.html">Pricing Tables</a></li>
                                                    <li><a href="login.html">Login</a></li>
                                                    <li><a href="register.html">Register</a></li>
                                                    <li><a href="404.html">404</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.html">Contact Us</a></li>
                                        </ul>
                                    </nav>
                                    <!-- END MAIN MENU -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER -->
