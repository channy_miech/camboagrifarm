

<!--BEGIN FOOTER -->
           <footer id="footer" class="footer-background">
               <div id="footer-top" class="container">
                   <div class="row">
                       <div class="block col-sm-3">
                           <div id="footer-about">
                               <img class="brend-logo" src="<?php bloginfo('template_url') ?>/assets/images/logo-white.png" alt="">
                               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.
                               </p>
                               <hr>
                               <ul class="socials-box pull-left">
                                   <li><a href="#"><i class="fa  fa-facebook"></i></a></li>
                                   <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                   <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                   <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                   <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                               </ul>
                           </div>
                       </div>
                       <div class="block col-sm-3">
                           <h3 class="footer-title text-uppercase">Contact US</h3>
                           <ul class="footer-contacts">
                               <li><i class="fa fa-phone"></i>+001 800 88 88</li>
                               <li><i class="fa fa-fax"></i>+001 800 88 88</li>
                               <li><i class="fa fa-envelope-o"></i> <a href="mailto:hello@wishhouse.com">hello@wishhouse.com</a></li>
                           </ul>
                           <h3 class="footer-title text-uppercase">Address</h3>
                           <ul class="footer-contacts">
                               <li>New York City,<br>Name, <br>8878 USA</li>
                           </ul>
                       </div>
                       <div class="block col-sm-3">
                           <h3 class="footer-title text-uppercase">Quick links</h3>
                           <ul>
                               <li><a href="#">Get a Fre Valuation</a></li>
                               <li><a href="#">Property Search</a></li>
                               <li><a href="#">Meet Our Experts</a></li>
                               <li><a href="#">FAQS</a></li>
                               <li><a href="#">Contact US 24/7</a></li>
                               <li><a href="#">Privacy Policy</a></li>
                               <li><a href="#">Cookis Policy</a></li>
                           </ul>
                       </div>
                       <div class="block col-sm-3">
                           <h3 class="footer-title text-uppercase">Latest News</h3>
                           <ul id="footer-recent-post">
                               <li>
                                   <a href="#">
                                       <div class="post-image"><img src="<?php bloginfo('template_url') ?>/assets/images/content/blog-thumbs-1.jpg" alt=""></div>
                                       <div class="info">
                                           <span class="data-time">14 Aug 2017</span>
                                           <span class="title">Spacious 3 Bedroom Semi-Detached for sale</span>
                                       </div>
                                   </a>
                               </li>
                               <li>
                                   <a href="#">
                                       <div class="post-image"><img src="<?php bloginfo('template_url') ?>/assets/images/content/blog-thumbs-2.jpg" alt=""></div>
                                       <div class="info">
                                           <span class="data-time">12 Aug 2017</span>
                                           <span class="title">Planning shake-up to get more homes built</span>
                                       </div>
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="logo-partners">
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-01.png" alt=""></a></div>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-02.png" alt=""></a></div>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-03.png" alt=""></a></div>
                           </div>
                           <hr>
                           <div class="logo-cards">
                               <p>We Accept Debit & Credit Cards</p>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/cards-01.png" alt=""></a></div>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/cards-02.png" alt=""></a></div>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/cards-03.png" alt=""></a></div>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/cards-04.png" alt=""></a></div>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/cards-05.png" alt=""></a></div>
                               <div class="item"><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/cards-06.png" alt=""></a></div>
                           </div>
                       </div>
                   </div>
               </div>

               <!--BEGIN COPYRIGHT -->
               <div id="copyright">
                   <a href="#" class="scrollTopButton">
                       <span class="button-square">
                           <i class="fa fa-angle-double-up"></i>
                       </span>
                   </a>
                   <div class="container">
                       <div class="row">
                           <div class="col-sm-12">
                               <span class="allright">2017 <strong class="t-color">Wish House </strong> All rights reserved. Designed by <strong class="t-color"> Colorfuldesign</strong></span>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- END COPYRIGHT -->
           </footer>
           <!-- END FOOTER -->
</div>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/common/jquery-1.x-git.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/common.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/bootstrap-progressbar.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.min.js"></script>

<!-- Datepiker Script -->
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/datepiker/js/moment.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/datepiker/js/bootstrap-datepicker.js"></script>
<!-- Hero Slide Scripts -->
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/xpro/js/slider.js"></script>
<!-- Template Scripts -->
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/scripts.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/contact-mail.js"></script>

<script type="text/javascript">
  // Hero Slide// --------------------------
  jQuery(document).ready(
      function() {
          var slider = new XPRO.Controls.Slider(null);
          slider.initSlider("scroller", {
                  "mode"              : "fade",
                  "dir"               : "left",
                  "iniWidth"          : 1200,
                  "iniHeight"         : 460,
                  "autoRun"           : true,
                  "interval"          : 6000,
                  "autoHeightMode"    : "maintainratio",
                  "thumbnails"        : true,
                  "stopOnHover"       : false,
                  "imageVAlign"       : "center",
                  "showProgress"      : false,
                  "enableNavigation"  : true,
               });

          jQuery(".xp-custom-navigation").on("click", function() {
              slider.forward();
              return false;
          });
      }
    );
</script>
</body>
</html>

  </body>

</html>
