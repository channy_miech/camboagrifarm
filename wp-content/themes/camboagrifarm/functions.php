<?php
// function add feature image
function nexuslablandsale_setup(){
  add_theme_support( 'post-thumbnails' );
}
add_action('after_setup_theme','nexuslablandsale_setup');

// function add style
function nexuslablandsale_style(){
    wp_register_style('style', get_template_directory_uri() . '/style.css', array(), '1.0');

    // Enqueue the style
    wp_enqueue_style('style');
}

add_action('wp_enqueue_scripts','nexuslablandsale_style');

// function add menu
function nexuslablandsale_menu(){
    register_nav_menus(array(
      'header-menu' => __('Header Menu','nexuslablandsale'),
      'social-menu' => __('Social Menu','nexuslablandsale')
    ));
}
add_action('init','nexuslablandsale_menu');

//function custom post Experts
function custom_expert_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Experts', 'Post Type General Name', 'camboagrifarm' ),
        'singular_name'       => _x( 'Expert', 'Post Type Singular Name', 'camboagrifarm' ),
        'menu_name'           => __( 'Experts', 'camboagrifarm' ),
        'parent_item_colon'   => __( 'Parent Expert', 'camboagrifarm' ),
        'all_items'           => __( 'All Experts', 'camboagrifarm' ),
        'view_item'           => __( 'View Expert', 'camboagrifarm' ),
        'add_new_item'        => __( 'Add New Expert', 'camboagrifarm' ),
        'add_new'             => __( 'Add New', 'camboagrifarm' ),
        'edit_item'           => __( 'Edit Expert', 'camboagrifarm' ),
        'update_item'         => __( 'Update Expert', 'camboagrifarm' ),
        'search_items'        => __( 'Search Expert', 'camboagrifarm' ),
        'not_found'           => __( 'Not Found', 'camboagrifarm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'camboagrifarm' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Experts', 'camboagrifarm' ),
        'description'         => __( 'Expert news and reviews', 'camboagrifarm' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'Experts', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_expert_post_type', 0 );
// function add logo header
function themeslug_theme_customizer( $wp_customize ) {
    // Fun code will go here
    $wp_customize->add_section( 'themeslug_logo_section' , array(
    'title'       => __( 'Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
    ) );

    $wp_customize->add_setting( 'themeslug_logo' );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Logo', 'themeslug' ),
    'section'  => 'themeslug_logo_section',
    'settings' => 'themeslug_logo',
) ) );
}
add_action( 'customize_register', 'themeslug_theme_customizer' );
