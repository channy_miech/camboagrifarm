<div class="container">
               <div class="divider col-sm-12"></div>
</div>
<!-- BEGIN FEATURED PROPERTY  -->
           <section class="featured-property">
               <div class="container">
                   <h2 class="section-title-border">Featured Properties</h2>
                   <div class="section-subtitle">
                       The internet is changing the way the property industry works.
                   </div>
                   <div class="edit-marginTop40 parallax-box ct-parallax-bg-img" data-image-src="<?php bloginfo('template_url') ?>/assets/images/content/featured-parallax.jpg">
                       <div class="mediaSection-box">
                           <div class="owl-carousel owl-featured owl-center-nav edit-paddingBoth20">
                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <label class="decor-label to-let">To Let </label>
                                       <a href="property-single.html">
                                           <div class="main-content">
                                               <div class="image-box">
                                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/property-17.jpg" alt="">
                                                   <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                               </div>
                                               <div class="main-info">
                                                   <div class="property-tilte">
                                                       2 Bedroom Apartmet for let
                                                   </div>
                                                   <div class="property-price">
                                                       <span>$750</span>
                                                   </div>
                                                   <hr>
                                                   <div class="property-desc">
                                                       Beautiful apartment in a great, very calm and safe place.
                                                   </div>
                                                   <div class="property-category">
                                                       <span class="decor"></span>
                                                       <span class="title">Our Tree Villas</span>
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                       <div class="property-bottom-info">
                                           <div class="icons">
                                               <span>
                                                   <i class="fa fa-bed"></i> 3
                                               </span>
                                               <span>
                                                   <i class="fa fa-bath" aria-hidden="true"></i> 2
                                               </span>
                                           </div>
                                           <div class="text">
                                               <span>250 sq ft</span>
                                               <span><i class="fa fa-heart-o"></i></span>
                                           </div>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->
                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <label class="decor-label let-agreed">Let Agreed </label>
                                       <a href="property-single.html">
                                           <div class="main-content">
                                               <div class="image-box">
                                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/property-2.jpg" alt="">
                                                   <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                               </div>
                                               <div class="main-info">
                                                   <div class="property-tilte">
                                                       Grande Dunes new villa
                                                   </div>
                                                   <div class="property-price">
                                                       <span>$2,000,000</span>
                                                   </div>
                                                   <hr>
                                                   <div class="property-desc">
                                                       Beautiful apartment in a great, very calm and safe place.
                                                   </div>
                                                   <div class="property-category">
                                                       <span class="decor"></span>
                                                       <span class="title">Our Tree Villas</span>
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                       <div class="property-bottom-info">
                                           <div class="icons">
                                               <span>
                                                   <i class="fa fa-bed"></i> 3
                                               </span>
                                               <span>
                                                   <i class="fa fa-bath" aria-hidden="true"></i> 2
                                               </span>
                                           </div>
                                           <div class="text">
                                               <span>750 sq ft</span>
                                               <span><i class="fa fa-heart-o"></i></span>
                                           </div>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->
                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <label class="decor-label to-let">To Let </label>
                                       <a href="property-single.html">
                                           <div class="main-content">
                                               <div class="image-box">
                                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/property-3.jpg" alt="">
                                                   <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                               </div>
                                               <div class="main-info">
                                                   <div class="property-tilte">
                                                       2 Bedroom Apaertment for let
                                                   </div>
                                                   <div class="property-price">
                                                       <span>$850</span>
                                                   </div>
                                                   <hr>
                                                   <div class="property-desc">
                                                       Beautiful apartment in a great, very calm and safe place.
                                                   </div>
                                                   <div class="property-category">
                                                       <span class="decor"></span>
                                                       <span class="title">Our Tree Villas</span>
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                       <div class="property-bottom-info">
                                           <div class="icons">
                                               <span>
                                                   <i class="fa fa-bed"></i> 3
                                               </span>
                                               <span>
                                                   <i class="fa fa-bath" aria-hidden="true"></i> 2
                                               </span>
                                           </div>
                                           <div class="text">
                                               <span>180 sq ft</span>
                                               <span><i class="fa fa-heart-o"></i></span>
                                           </div>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->
                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <label class="decor-label let-agreed">Let Agreed </label>
                                       <a href="property-single.html">
                                           <div class="main-content">
                                               <div class="image-box">
                                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/property-4.jpg" alt="">
                                                   <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                               </div>
                                               <div class="main-info">
                                                   <div class="property-tilte">
                                                       4 bedroom mobile home for sale
                                                   </div>
                                                   <div class="property-price">
                                                       <span>$350,000</span>
                                                   </div>
                                                   <hr>
                                                   <div class="property-desc">
                                                       Beautiful apartment in a great, very calm and safe place.
                                                   </div>
                                                   <div class="property-category">
                                                       <span class="decor"></span>
                                                       <span class="title">Flat</span>
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                       <div class="property-bottom-info">
                                           <div class="icons">
                                               <span>
                                                   <i class="fa fa-bed"></i> 7
                                               </span>
                                               <span>
                                                   <i class="fa fa-bath" aria-hidden="true"></i> 3
                                               </span>
                                           </div>
                                           <div class="text">
                                               <span>650 sq ft</span>
                                               <span><i class="fa fa-heart-o"></i></span>
                                           </div>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->
                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <label class="decor-label for-sale">For Sale </label>
                                       <a href="property-single.html">
                                           <div class="main-content">
                                               <div class="image-box">
                                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/property-5.jpg" alt="">
                                                   <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                               </div>
                                               <div class="main-info">
                                                   <div class="property-tilte">
                                                       6 Bedroom Bungalow for sale
                                                   </div>
                                                   <div class="property-price">
                                                       <span class="old-price">$500,000</span>
                                                       <span>$450,000</span>
                                                   </div>
                                                   <hr>
                                                   <div class="property-desc">
                                                       Beautiful apartment in a great, very calm and safe place.
                                                   </div>
                                                   <div class="property-category">
                                                       <span class="decor"></span>
                                                       <span class="title">Our Tree Villas</span>
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                       <div class="property-bottom-info">
                                           <div class="icons">
                                               <span>
                                                   <i class="fa fa-bed"></i> 8
                                               </span>
                                               <span>
                                                   <i class="fa fa-bath" aria-hidden="true"></i> 4
                                               </span>
                                           </div>
                                           <div class="text">
                                               <span>1200 sq ft</span>
                                               <span><i class="fa fa-heart-o"></i></span>
                                           </div>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <!-- END FEATURED PROPERTY  -->

           <!-- BEGIN PROPERTY EXPERT  -->
           <section class="edit-padding0 edit-paddingTop20">
               <div class="container">
                   <h4 class="property-expert-title">Property Experts</h4>
               </div>
               <div class="property-expert ct-background-color" data-bgcolor="#fbfbfb" data-borderTop="1px solid #e2e2e2" data-borderBottom="1px solid #e2e2e2">
                   <div class="container">
                       <div class="row">
                           <div class="owl-carousel owl-property-expert owl-top-nav"> <!-- On special-menu list  -->
                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <div class="col-md-4">
                                           <div class="image"><img src="<?php bloginfo('template_url') ?>/assets/images/content/expert-1.png" alt=""></div>
                                       </div>
                                       <div class="col-md-4">
                                           <h4 class="title">Ruzena Bajcsy</h4>
                                           <span>The standard chunk of Lorem Ipsum</span>
                                           <ul class="socials-box pull-left">
                                               <li><a href="#"><i class="fa  fa-facebook"></i></a></li>
                                               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                               <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                           </ul>
                                           <div class="progress-boxes">
                                               <h6 class="title">Expensive Property</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="90"></div>
                                               </div>
                                               <h6 class="title">Relatively cheap Properties</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="60"></div>
                                               </div>
                                               <h6 class="title">Cheap valuable rental properties</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="35"></div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-md-4">
                                           <h5 class="title center-text">Find your Local Property Expert</h5>
                                           <span class="center-text">for the very best service</span>
                                           <div class="search-box center-text">
                                               <form class="search-form">
                                                   <input type="text" placeholder="Search...">
                                                   <button type="submit"><i class="fa fa-search"></i></button>
                                               </form>
                                           </div>
                                           <hr>
                                           <p class="center-text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't
                                               look even slightly believable. </p>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->

                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <div class="col-md-4">
                                           <div class="image"><img src="<?php bloginfo('template_url') ?>/assets/images/content/expert-2.png" alt=""></div>
                                       </div>
                                       <div class="col-md-4">
                                           <h4 class="title">Robert Anthony</h4>
                                           <span>The standard chunk of Lorem Ipsum</span>
                                           <ul class="socials-box pull-left">
                                               <li><a href="#"><i class="fa  fa-facebook"></i></a></li>
                                               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                               <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                           </ul>
                                           <div class="progress-boxes">
                                               <h6 class="title">Expensive Property</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="65"></div>
                                               </div>
                                               <h6 class="title">Relatively cheap Properties</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="85"></div>
                                               </div>
                                               <h6 class="title">Cheap valuable rental properties</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="35"></div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-md-4">
                                           <h5 class="title center-text">Find your Local Property Expert</h5>
                                           <span class="center-text">for the very best service</span>
                                           <div class="search-box center-text">
                                               <form class="search-form">
                                                   <input type="text" placeholder="Search...">
                                                   <button type="submit"><i class="fa fa-search"></i></button>
                                               </form>
                                           </div>
                                           <hr>
                                           <p class="center-text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't
                                               look even slightly believable. </p>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->

                               <div class="item"><!-- On item  -->
                                   <div class="item-box">
                                       <div class="col-md-4">
                                           <div class="image"><img src="<?php bloginfo('template_url') ?>/assets/images/content/expert-3.png" alt=""></div>
                                       </div>
                                       <div class="col-md-4">
                                           <h4 class="title">Thomas Albert</h4>
                                           <span>The standard chunk of Lorem Ipsum</span>
                                           <ul class="socials-box pull-left">
                                               <li><a href="#"><i class="fa  fa-facebook"></i></a></li>
                                               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                               <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                           </ul>
                                           <div class="progress-boxes">
                                               <h6 class="title">Expensive Property</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="35"></div>
                                               </div>
                                               <h6 class="title">Relatively cheap Properties</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="70"></div>
                                               </div>
                                               <h6 class="title">Cheap valuable rental properties</h6>
                                               <div class="progress progress-e">
                                                   <div class="progress-bar progress-bar-e" role="progressbar" data-transitiongoal="55"></div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-md-4">
                                           <h5 class="title center-text">Find your Local Property Expert</h5>
                                           <span class="center-text">for the very best service</span>
                                           <div class="search-box center-text">
                                               <form class="search-form">
                                                   <input type="text" placeholder="Search...">
                                                   <button type="submit"><i class="fa fa-search"></i></button>
                                               </form>
                                           </div>
                                           <hr>
                                           <p class="center-text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't
                                               look even slightly believable. </p>
                                       </div>
                                   </div>
                               </div><!-- OFF item  -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <!-- END PROPERTY EXPERT  -->

           <!-- BEGIN FROM THE BLOG  -->
           <section class="blog-content edit-padding0 colorsidebar">
               <div class="container">
                   <div class="flex-row row">
                       <div class="main col-md-8">
                           <h3 class="section-title-border">From the Blog</h3>
                           <div class="articleBox edit-marginBottom40 edit-marginTop20">
                               <div class="articleBox-body"><!-- ON article -->
                                   <a href="#"><div class="articleBox-media">
                                           <img src="<?php bloginfo('template_url') ?>/assets/images/content/blog-02.jpg" alt="">
                                           <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                       </div></a>
                                   <div class="articleBox-info">
                                       <div class="articleBox-titles">
                                           <div class="articleTop-info">
                                               <span><i class="fa fa-calendar"></i> August 07th, 2017</span> <span><i class="fa fa-comments-o"></i> 3</span>
                                               <span>2017 in Loans, <strong>Wish House</strong></span>
                                           </div>
                                           <a href="#">The Future of Property Prices & Rents in the South East</a>
                                       </div>
                                       <div class="articleBox-description">
                                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. <a href="#">Read more</a>
                                       </div>
                                   </div>
                               </div>
                               <div class="articleBox-body"><!-- ON article -->
                                   <a href="#"><div class="articleBox-media">
                                           <img src="<?php bloginfo('template_url') ?>/assets/images/content/blog-03.jpg" alt="">
                                           <div class="overlay-box"><div class="overlay"></div><i class="fa fa-play-circle"></i></div>
                                       </div></a>
                                   <div class="articleBox-info">
                                       <div class="articleBox-titles">
                                           <div class="articleTop-info">
                                               <span><i class="fa fa-calendar"></i> June 09th, 2017</span> <span><i class="fa fa-comments-o"></i> 3</span>
                                               <span>2017 in Loans, <strong>Wish House</strong></span>
                                           </div>
                                           <a href="#">Planning shake-up to get more homes built</a>
                                       </div>
                                       <div class="articleBox-description">
                                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. <a href="#">Read more</a>
                                       </div>
                                   </div>
                               </div>
                               <div class="articleBox-body"><!-- ON article -->
                                   <a href="#"><div class="articleBox-media">
                                           <img src="<?php bloginfo('template_url') ?>/assets/images/content/blog-01.jpg" alt="">
                                           <div class="overlay-box"><div class="overlay"></div><i class="fa fa-search"></i></div>
                                       </div></a>
                                   <div class="articleBox-info">
                                       <div class="articleBox-titles">
                                           <div class="articleTop-info">
                                               <span><i class="fa fa-calendar"></i> Juny 23th, 2017</span> <span><i class="fa fa-comments-o"></i> 3</span>
                                               <span>2017 in Loans, <strong>Wish House</strong></span>
                                           </div>
                                           <a href="#">The Property Market Post Brexit</a>
                                       </div>
                                       <div class="articleBox-description">
                                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. <a href="#">Read more</a>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="sidebar col-md-4 colorsidebar">
                           <h3 class="section-title-border">About Us</h3>
                           <div class="sidebarAbout-box">
                               <div class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin <strong>literature from</strong> 45 BC, making it over 2000 years old. </div>
                           </div>
                           <div class="sidebarAds-box">
                               <h3 class="ads-title">Looking to Sell Your Property?</h3>
                               <div class="ads-desc">All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary on the Internet.</div>
                               <a class="btn btn-primary btn-o-primary btn-sm" href="#">Book a Valuation <i class="fa fa-chevron-right"></i></a>
                           </div>
                           <div class="sidebarAds-box">
                               <h3 class="ads-title">Are You a Landlord?</h3>
                               <div class="ads-desc">All the Lorem Ipsum geneartors on the Internet tend to repeat predefined chunks as necessary on the Internet.</div>
                               <a class="btn btn-primary btn-o-primary btn-sm" href="#">Request Appraisal <i class="fa fa-chevron-right"></i></a>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <!-- END FROM THE BLOG -->

           <!-- BEGIN TESTIMONIALS  -->
           <section class="testimonials parallax-box">
               <div class="mediaSection-box">
                   <div class="container">
                       <div class="owl-carousel owl-testimonials"> <!-- On testimonials slide  -->
                           <div class="item-bg-box"><!-- On item  -->
                               <div class="thumb-section">
                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/testimonials-03.jpg" alt="">
                                   <h3 class="name">John Parkinson</h3>
                                   <div class="town">New York</div>
                               </div>
                               <div class="desc">
                                   <p><i class="fa fa-quote-left"></i>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need<i class="fa fa-quote-right"></i></p>
                               </div>
                           </div><!-- OFF item  -->
                           <div class="item-bg-box"><!-- On item  -->
                               <div class="thumb-section">
                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/testimonials-01.jpg" alt="">
                                   <h3 class="name">Sarah Boysen</h3>
                                   <div class="town">California</div>
                               </div>
                               <div class="desc">
                                   <p><i class="fa fa-quote-left"></i>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need<i class="fa fa-quote-right"></i></p>
                               </div>
                           </div><!-- OFF item  -->
                           <div class="item-bg-box"><!-- On item  -->
                               <div class="thumb-section">
                                   <img src="<?php bloginfo('template_url') ?>/assets/images/content/testimonials-02.jpg" alt="">
                                   <h3 class="name">Goodall Family</h3>
                                   <div class="town">Virginia</div>
                               </div>
                               <div class="desc">
                                   <p><i class="fa fa-quote-left"></i>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need<i class="fa fa-quote-right"></i></p>
                               </div>
                           </div><!-- OFF item  -->

                       </div><!-- OFF Testimoials slide  -->
                   </div>
               </div>
           </section>
           <!-- END TESTIMONIALS  -->

           <!-- BEGIN SPECIAL PROPERTIES -->
           <section class="specialBoxses sectionBox-color">
               <div class="container">
                   <h2 class="section-title-border whiteTitleBorder">Special Properties</h2>
                   <div class="section-subtitle white">
                       The internet is changing the way the property industry works.
                   </div>
                   <div class="col-md-10 col-md-offset-1 edit-marginTop30">
                       <div class="col-md-6">
                           <div class="special-item-box">
                               <label class="decor-label for-sale">For Sale </label>
                               <div class="main-content">
                                   <a href="property-single.html">
                                       <div class="image-box">
                                           <img src="<?php bloginfo('template_url') ?>/assets/images/content/property-1.jpg" alt="">
                                           <div class="mini-info">
                                               <div class="property-price">
                                                   <span>$250,000</span>
                                               </div>
                                               <div class="icons">
                                                   <span>
                                                       <i class="fa fa-bed"></i> 3
                                                   </span>
                                                   <span>
                                                       <i class="fa fa-bath" aria-hidden="true"></i> 2
                                                   </span>
                                               </div>
                                               <div class="text">
                                                   <span>350 sq ft</span>
                                               </div>
                                           </div>
                                       </div>
                                   </a>
                                   <div class="main-info">
                                       <a href="property-single.html">
                                           <div class="property-tilte">Luxury Apartment with great views</div>
                                       </a>
                                       <div class="property-desc">
                                           There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="special-item-box">
                               <label class="decor-label for-sale">For Sale </label>
                               <div class="main-content">
                                   <a href="property-single.html">
                                       <div class="image-box">
                                           <img src="<?php bloginfo('template_url') ?>/assets/images/content/property-2.jpg" alt="">
                                           <div class="mini-info">
                                               <div class="property-price">
                                                   <span>$950,000</span>
                                               </div>
                                               <div class="icons">
                                                   <span>
                                                       <i class="fa fa-bed"></i> 3
                                                   </span>
                                                   <span>
                                                       <i class="fa fa-bath" aria-hidden="true"></i> 2
                                                   </span>
                                               </div>
                                               <div class="text">
                                                   <span>350 sq ft</span>
                                               </div>
                                           </div>
                                       </div>
                                   </a>
                                   <div class="main-info">
                                       <a href="property-single.html">
                                           <div class="property-tilte">Spacious 3 Bedroom Semi-Detached for sale</div>
                                       </a>
                                       <div class="property-desc">
                                           There are many variations of passages of Lorem Ipsum available, but the majority have suffered.
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <!-- END SPECIAL PROPERTIES -->


            <!-- BEGIN OUR PAERTNERS  -->
            <section class="our-partners">
                <div class="container">
                    <h2 class="section-title-border">Our Partners</h2>
                    <div class="section-subtitle">
                        The internet is changing the way the property industry works.
                    </div>
                    <div class="edit-marginTop40">
                        <ul class="owl-carousel owl-ourPartners owl-center-nav">
                            <li><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-b04.png" alt=""></a></li>
                            <li><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-b02.png" alt=""></a></li>
                            <li><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-b01.png" alt=""></a></li>
                            <li><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-b05.png" alt=""></a></li>
                            <li><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-b03.png" alt=""></a></li>
                            <li><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-b01.png" alt=""></a></li>
                            <li><a href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/content/partner-b03.png" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- END OUR PAERTNERS  -->

            <!-- BEGIN NEWSLETTER  -->
           <section class="newsletter decor-bg">
               <div class="container">
                   <div class="center-text col-md-12">
                       <h2 class="big-title">Subscripe Newsletter</h2>
                       <span class="small-title">and be notified about new locations</span>
                       <div class="nl-box">
                           <form method="post" id="newsletterform" name="newsletterform" class="newsletter-form" action="assets/mail/newsletter.php">
                               <input type="email" id="nl-email" class="form-control" name="nl-email" placeholder="Your email address">
                               <input type="submit" name="nl-submit" id="nl-submit" class="btn btn-sm" value="Submit">
                           </form>
                           <div id="nl-message"></div>
                       </div>
                   </div>
               </div>
           </section>
           <!-- END NEWSLETTER  -->
