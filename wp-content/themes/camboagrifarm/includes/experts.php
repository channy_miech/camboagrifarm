<?php
/*
* Creating a function to create our CPT
*/

function custom_expert_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Experts', 'Post Type General Name', 'camboagrifarm' ),
        'singular_name'       => _x( 'Expert', 'Post Type Singular Name', 'camboagrifarm' ),
        'menu_name'           => __( 'Experts', 'camboagrifarm' ),
        'parent_item_colon'   => __( 'Parent Expert', 'camboagrifarm' ),
        'all_items'           => __( 'All Experts', 'camboagrifarm' ),
        'view_item'           => __( 'View Expert', 'camboagrifarm' ),
        'add_new_item'        => __( 'Add New Expert', 'camboagrifarm' ),
        'add_new'             => __( 'Add New', 'camboagrifarm' ),
        'edit_item'           => __( 'Edit Expert', 'camboagrifarm' ),
        'update_item'         => __( 'Update Expert', 'camboagrifarm' ),
        'search_items'        => __( 'Search Expert', 'camboagrifarm' ),
        'not_found'           => __( 'Not Found', 'camboagrifarm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'camboagrifarm' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Experts', 'camboagrifarm' ),
        'description'         => __( 'Expert news and reviews', 'camboagrifarm' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'Experts', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_expert_post_type', 0 );
